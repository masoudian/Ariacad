#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.utils.html import escape
import sys

import os
from django.core.files.uploadedfile import SimpleUploadedFile
from PIL import Image
from website.storage import OverwriteStorage 
from io import BytesIO


from datetime import date
from django.db.models.fields.related import ForeignKey
from django.db.models.fields import PositiveSmallIntegerField, DateTimeField
#from pip.cmdoptions import editable
from django.template.defaultfilters import default
from cloudinary.api import update

def cover_img_path(instance, filename):
    extension = filename.split('.')[-1]
    fname = 'cover' + "." + extension #instance.file_extension 
    return '/'.join(['planfiles', instance.plan_code, fname])

class Plan(models.Model):
    # Sharhe code ra baadan ezafe mikonim
    plan_code = models.CharField("کد نقشه",max_length=30)
    
    plan_description = models.TextField("توصیف نقشه", max_length=20000, null=True, blank = 'True')
    # toole pishravi
    plan_length = models.DecimalField("طول نقشه",max_digits=10, decimal_places=2)
    #arze naghshe
    plan_width = models.DecimalField("عرض نقشه",max_digits=10, decimal_places=2)
    #zirbana
    plan_substructure = models.DecimalField("زیربنای نقشه",max_digits=10, decimal_places=2)
    # krookie zamin: shomali, jonoobi, ...
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6
    krooki = (
        (A, 'جنوبی'),
        (B, 'شمالی'),
        (C, 'شمالی-جنوبی'),
        (D, 'دو نبش'),
        (E, 'بر به کوچه'),
        (F, 'نورگیری از چهار طرف'),  #a ویلایی
                    )
    plan_krooki = models.PositiveSmallIntegerField("کروکی/وضعیت ساختمان", choices=krooki, null=True)
    # entekhabhaye teedade tabaghat
    stories = ( 
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         )
    #teedade tabaghat
    plan_stories = models.PositiveSmallIntegerField("تعداد طبقات", choices=stories)
    # entekhabhaye teedade vahed
    units = ( 
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         )
    #teedade vahed
    plan_units = models.PositiveSmallIntegerField("تعداد واحدها",choices=units)
    #teedad mashini ke dar parking ja mishavad
    plan_garage = models.PositiveSmallIntegerField("تعداد پارکینگ")
    #plan pilot ast ya zirzamin, meghdare true pilot budan ra neshan midahad
    plan_pilot = models.BooleanField("پیلوت؟")
    # plan apartmani ast ya mamuli
    plan_apartment = models.BooleanField("آپارتمان؟")
    # parvane i budane plan, meghdare true parvanei budan ra neshan midahad
    plan_issued = models.BooleanField("issued?/پروانه ای؟")
    #entekhabhaye teedade otaghha
    rooms = ( 
         (0,0),
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         )
    # teedade otagh vahede asli  ---  واحد اصلی  است"A". -> این فیلد باید در زمان سیو شدن جدول واحد پر شود
    plan_rooms = models.PositiveIntegerField("تعداد اتاق واحد اصلی (واحد A)",choices=rooms, null=True)
    # meghdare true dublex budane plan ra neshan midahad
    plan_dublex = models.BooleanField("دوبلکس؟")
    # meghdare true dashtan voroodi mojazza ra neshan midahad
    plan_separateEntry = models.BooleanField("در مجزا دارد؟", default = False)
    # teedade komd divari
    #plan_closet = models.PositiveSmallIntegerField("how many closets?")
    # emkanate ezafe
    plan_extraFacility = models.CharField("امکانات اضافه", max_length=200, null=True, blank = 'True')
    
    #plan_price = models.PositiveIntegerField ("قیمت نقشه", default =0, null=True, blank=True)
    # aya asansor darad?
    plan_elevator = models.BooleanField("آسانسور دارد؟")
    # emtiaze karbaran be plan
    plan_point = models.DecimalField("point of plan", max_digits=2, decimal_places=1, default=0)
    # teedade bazdid
    plan_visits = models.PositiveIntegerField("number of visits", default=0)
    # teedade foroosh
    plan_sales = models.PositiveIntegerField("number of sales", default=0)
    # tarikhe ezafe shodane plan
    plan_add_date = models.DateTimeField("plan add date", auto_now_add=True, null=True)
    # naghshehaye setare dar
    plan_special = models.BooleanField("is special?", default=0)
    #baraye vaghti ke sefareshe khas anjam shode va be archive ezafe mishavad
    plan_show = models.BooleanField("نمایش داده شود؟", default=True)
    
    plan_pillar = models.BooleanField("ستون گذاری دارد؟", default=False)
    plan_class = models.ForeignKey('PlanClass')
    cover_img = models.ImageField("کاور نقشه", upload_to=cover_img_path, null = True, blank=True) 
    
    def save(self, *args, **kwargs):
        try:
            # thumbnail
            if self.cover_img: 
                thumb_size = (300, 200)
                cover_image = Image.open(self.cover_img)
                cover_image.thumbnail(thumb_size)     # it is modified inplace
             
                # fetch image into memory
                temp_handle = BytesIO()
                cover_image.save(temp_handle, 'png')
                temp_handle.seek(0)
                # save it
                file_name, file_ext = os.path.splitext(self.cover_img.name.rpartition('/')[-1])
                suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read())
                self.cover_img.save(file_name + file_ext, suf, save=False)
             
            super(Plan, self).save(*args, **kwargs)
        except ImportError:
            pass   
        
    def __str__(self):
        return self.plan_code
    
    #===========================================================================
    # def generate_filename(self):
    #     url = "files/users/%s/%s" % (self.plan, filename)
    #     return url
    #===========================================================================



#===============================================================================
# class PlanCover(models.Model):
#     plan = models.OneToOneField(Plan, on_delete=models.CASCADE)
#     cover_img = models.FileField("کاور نقشه", upload_to=cover_img_path, null = True)#, blank=True) 
#     
#     def save(self, *args, **kwargs):
#         try:
#             # thumbnail
#             if self.cover_img:  
#                 thumb_size = (300, 200)
#                 cover_image = ImageObj.open(self.cover_img)
#                 cover_image.thumbnail(thumb_size)     # it is modified inplace
#                 
#                 # fetch image into memory
#                 temp_handle = BytesIO()
#                 cover_image.save(temp_handle, 'png')
#                 temp_handle.seek(0)
#                 # save it
#                 file_name, file_ext = os.path.splitext(self.cover_img.name.rpartition('/')[-1])
#                 suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read())
#                 self.cover_img.save(file_name + file_ext, suf, save=False)
#             
#             super(PlanCover, self).save(*args, **kwargs)
#         except ImportError:
#             pass
#             
#             #PlanCover(plan=self.plan).save()
#===============================================================================
    
    
##################################################################################################################

class Story(models.Model):
    stories = ( 
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         ) 
    plan=models.ForeignKey('Plan', on_delete=models.CASCADE)
    storyNumber=models.PositiveSmallIntegerField("شماره ی طبقه", choices = stories)
    units = ( 
         (0,0),
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         ) 
    numberOfUnits=models.PositiveSmallIntegerField("تعداد واحد", choices = units, null=True)
    
    unitTypes = models.CharField("تیپ های واحدها", max_length=30, default='')
    
    
    def saveunit(self):
        self.save()
##################################################################################################################
        
#===============================================================================
# class Unit(models.Model):   #not in use anymore - replaced by unit_type
#     stories = ( 
#          (1,1),
#          (2,2),
#          (3,3),
#          (4,4),
#          (5,5),
#          (6,6),
#          ) 
#     plan = models.ForeignKey('Plan', on_delete=models.CASCADE)
#     storyNumber=models.PositiveSmallIntegerField("شماره ی طبقه", choices=stories)
#     unitSize=models.FloatField("typical unit size/متراژ واحد")
#     rooms = ( 
#          (1,1),
#          (2,2),
#          (3,3),
#          (4,4),
#          (5,5),
#          (6,6),
#          ) 
#     numberOfRooms=models.PositiveSmallIntegerField("تعداد اتاق ها", choices = rooms)
#     closet = models.PositiveSmallIntegerField("تعداد کمد", default=0)
#     def saverooms(self):
#         self.save()
#===============================================================================
##################################################################################################################

class UnitType(models.Model):
    
    plan=models.ForeignKey('Plan', on_delete=models.CASCADE)  
    name = models.CharField("اسم تیپ", max_length=1)
    unitSize=models.FloatField("typical unit size/متراژ واحد")
    rooms = ( 
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
         ) 
    numberOfRooms=models.PositiveSmallIntegerField("تعداد اتاق ها", choices = rooms)
    
    def saverooms(self):
        self.save()
##################################################################################################################

def large_file_path(instance, filename):
    return '/'.join(['planfiles', instance.plan.plan_code, filename])
def thumb_file_path(instance, filename):
    return '/'.join(['planfiles', instance.plan.plan_code, 'thumb_'+filename])
##############################
class PlanFile(models.Model):      
    plan = models.ForeignKey('plan', on_delete=models.CASCADE)
  
    large_img = models.FileField("تصویر نقشه", upload_to=large_file_path,  null = True)
    thumb_img = models.FileField(upload_to=thumb_file_path,  null = True, blank=True, editable=False)
    #eidtable=false is for django-admin to not to show the field
    
    
    def save(self, *args, **kwargs):
        try:
            # thumbnail
            img_lrg_size = (1400, 1400)
            img_thmb_size = (300, 300)  # dimensions
            
            original_img = Image.open(self.large_img)
            image = original_img.copy()
            thumb = original_img.copy()  # thumbnail module modifies the original image
        
            # create a thumbnail
            thumb.thumbnail(img_thmb_size)
            image.thumbnail(img_lrg_size)
            
            ######for large image
            # fetch image into memory
            temp_handle = BytesIO()
            image.save(temp_handle, 'png')
            temp_handle.seek(0)
            # save it
            file_name, file_ext = os.path.splitext(self.large_img.name.rpartition('/')[-1])
            suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read())
            self.large_img.save(file_name + file_ext, suf, save=False)
            
            ######for thumbnail
            # fetch image into memory
            temp_handle = BytesIO()
            thumb.save(temp_handle, 'png')
            temp_handle.seek(0)
            # save it
            file_name, file_ext = os.path.splitext(self.large_img.name.rpartition('/')[-1])
            suf = SimpleUploadedFile(file_name + file_ext, temp_handle.read())
            self.thumb_img.save(file_name + file_ext, suf, save=False)
            
            
            super(PlanFile, self).save(*args, **kwargs)
        except ImportError:
            pass   
    
    def __str__(self):
        return str(self.id)
##################################################################################################################     
        
class Threed_prototype(models.Model):
        categories = ( 
         (1,'main'),
         (2,'nema'),
         (3,'boresh'),
         (4,'hayat'),
         (5,'other1'),
         (6,'other2'),
         )
        category = models. PositiveSmallIntegerField("category name", choices = categories, null = True)
##################################################################################################################
        
class PlanComment(models.Model):
    user = models. PositiveSmallIntegerField(null='True', default = 0 )
    plan = models. ForeignKey ('plan', on_delete=models.CASCADE)
    comment_text = models.CharField (null='True', max_length=10000)
    comment_date = models.DateTimeField("plan add date", auto_now_add=True, null=True)
    person_name = models.CharField (null='True', max_length=100)
    person_email = models.EmailField(null='True')
    
    commnet_status = ( 
         (0,'منتظر'),
         (1,'تایید'),
         (2,'عدم تایید'),
         )
    comment_confirmation = models.PositiveSmallIntegerField("وضعیت", choices = commnet_status, default=0)
    
    def saveplanComments(self):
        self.save()
##################################################################################################################

class FavPlan(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    plan = models.ForeignKey('Plan', on_delete=models.CASCADE)
    createdTime = models.DateTimeField(auto_now_add=True, null=True)   
    
##################################################################################################################
'''price per square meter for each class type'''
class PlanClass(models.Model):
    plan_class = models.CharField(max_length=1)
    price = models.IntegerField()
    
    def __str__(self):
        return self.plan_class + ": " + str(self.price)

##################################################################################################################
''' each entry shows the prices for cad and post package in specific time,
    the LAST ENTRY is the CURRENT PRICES (others are just kept archived).
    cad: percents -- the final price will be percents of plan price
    post: a fixed price 
    '''
class ExtraPrice(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    cad_price_percent = models.PositiveIntegerField()
    post_price = models.PositiveIntegerField()
    
##################################################################################################################
''' پیش پرداخت چند درصد قیمت اولیه ی نقشه باشد 
'''
class PrepaymentAmount(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    percents = models.PositiveSmallIntegerField()

##################################################################################################################
''' discount and sales amounts
'''
# class Discount():
    
    
    
    
