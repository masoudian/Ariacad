#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.contrib.auth import get_user_model
from django.db.models import Q
import functools
from operator import or_

from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from django.utils import timezone

from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    RetrieveAPIView, 
    RetrieveUpdateAPIView,
    UpdateAPIView
    )



from rest_framework.permissions import (
    AllowAny,
    )

#from .permissions import IsProfileOwner

from product.models import Plan, FavPlan, ExtraPrice, PlanFile
from .functions import read_from_excel_v1, upload_zip_file, unzip_images, fill_images
from .pagination import PostPageNumberPagination
#from django_filters.filterset import FilterSet
import django_filters

User = get_user_model()

from .serializers import ( 
    AddRemoveFavPlanSerializer,
    ExtraPriceSerializer,
    FavPlanSerializer,
    PlanDetailSerializer,
    PlanListSerializer,
    PlanSummarySerailizer,
    
    ExcelSerializer,
    ZipSerializer,
    )


class PlanDetailAPIView(RetrieveAPIView):
    queryset = Plan.objects.all()
    permission_classes = [AllowAny]
    serializer_class = PlanDetailSerializer
    
    def get_serializer_context(self):
        if self.request.user.is_anonymous():
            user = None
        else:
            user = self.request.user
        context = {'user': user, 'request': self.request}
        return context


#===============================================================================
# class NumberInFilter(django_filters.BaseInFilter, django_filters.NumberFilter):
#     ''' not in use right now! '''
#     def filter(self, qs, value):
#         # CSV-based filters receive a list of values
#         if 4 not in value:
#             #return  qs.filter( **{  self.name: value, })
#             return super(NumberInFilter,value).filter(qs, value)
#         value.remove(4)
# 
#         # a little bit more verbose, but doesn't require hardcoding the name.
#         # eg, `qs.filter(Q(object__in=value) | Q(object__gte=4))`
#         q1 = Q(**{'%s__%s' % (self.name, 'in'): value})
#         q2 = Q(**{'%s__%s' % (self.name, 'gte'): 4})
#         return qs.filter(q1 | q2)
#===============================================================================

class MyMultipleChoiceFilter(django_filters.MultipleChoiceFilter):
    def get_filter_predicate(self, value):
        if value == u'4':
            return {'%s__gte' % self.name: value}
        return super(MyMultipleChoiceFilter, self).get_filter_predicate(value)

multiple_choice = ( 
         (1,1),
         (2,2),
         (3,3),
         (4,4),
         )

lighting_choices = (
        (1,1),
         (2,2),
         (3,3),
         (4,4),
         (5,5),
         (6,6),
                    )
class PlanFilter(FilterSet):
    '''
    this filters all the fields except for plan and lot's length and width
    it recieves the qs from url parameters, 
    the param names are the same as model fields, except the str, un, rm, gr.
    '''
    story = MyMultipleChoiceFilter(name='plan_stories', choices=multiple_choice)#, method='story_filter') 
    #unit = NumberInFilter(name='plan_units', lookup_expr='in')
    unit = MyMultipleChoiceFilter(name='plan_units', choices=multiple_choice)
    room =  MyMultipleChoiceFilter(name='plan_rooms', choices=multiple_choice)
    garage =  MyMultipleChoiceFilter(name='plan_garage', choices=multiple_choice)
    lighting = django_filters.ChoiceFilter(name='plan_krooki', choices=lighting_choices)
    separateEntry = django_filters.BooleanFilter(name='plan_separateEntry')
    pilot = django_filters.BooleanFilter(name='plan_pilot')
    elevator = django_filters.BooleanFilter(name='plan_elevator')
    dublex = django_filters.BooleanFilter(name='plan_dublex')
    apartment = django_filters.BooleanFilter(name='plan_apartment')
    
    # plan_code =
    # extraFacility =
    
    class Meta:
        model = Plan 
        fields = ['story', 'unit', 'room', 'garage', 'separateEntry', 
                  'pilot', 'elevator', 'dublex', 'apartment']
        #=======================================================================
        # exclude = ['plan_description', 'plan_substructure',
        #            'plan_issued', 'plan_point', 'plan_visits', 'plan_sales', 
        #            'plan_add_date', 'plan_special', 'plan_show', 'plan_stories',
        #            'plan_units', 'plan_rooms', 'plan_garage', 'cover_img', 
        #            'plan_pillar', 'plan_class', 'plan_length', 'plan_width', ]
        #=======================================================================
    

# search module 
class PlanListAPIView(ListAPIView):
    permission_classes = [AllowAny]
    queryset = Plan.objects.filter(plan_show=True)
    serializer_class = PlanListSerializer
    pagination_class = PostPageNumberPagination
    
    filter_backends = (DjangoFilterBackend,)
    filter_class = PlanFilter
    
    def get_serializer_context(self):
        if self.request.user.is_anonymous():
            user = None
        else:
            user = self.request.user
        context = {'user': user, 'request': self.request}
        return context
    
    def get_queryset(self):  
        qs = Plan.objects.filter(plan_show=True)
        request = self.request
        
        
        # get the min & max dimensions from url parameter
        plength = request.GET.getlist('plength', '') # plan w,l
        pwidth = request.GET.getlist('pwidth', '')
        
        llength = request.GET.get('llength', '') # lot w,l
        lwidth = request.GET.get('lwidth', '')
        
        pqs = Plan.objects.none()
        lqs = Plan.objects.none()
        if plength and pwidth:
            # add a -/+15% range to length 
            minL = float(plength[0])*0.85
            maxL = float(plength[1])*1.15
            # add a -/+10% range to width 
            minW = float(pwidth[0])*0.9
            maxW = float(pwidth[1])*1.1
            
            pqs = qs.filter(plan_length__lte=maxL, plan_length__gte=minL)\
                    .filter(plan_width__lte=maxW, plan_width__gte=minW)
                    
        elif llength and lwidth:
            # calculte 50--70% of the lot length, then add +/-15% to it
            minL = float(llength) * 0.5 * 0.85
            maxL = float(llength) * 0.7 * 1.15
            # and add +/-10% to width
            minW = float(lwidth) * 0.9
            maxW = float(lwidth) * 1.1
            
            lqs = qs.filter(plan_length__lte=maxL, plan_length__gte=minL)\
                    .filter(plan_width__lte=maxW, plan_width__gte=minW)

#===============================================================================
#         plength = request.GET.get('plength', '') # plan w,l
#         pwidth = request.GET.get('pwidth', '')
#         llength = request.GET.get('llength', '') # lot w,l
#         lwidth = request.GET.get('lwidth', '')
# 
#         pqs = lqs = Plan.objects.none()
#         if plength and pwidth:
#              pqs = qs.filter(plan_length__lte=1.15*float(plength), plan_length__gte=0.85*float(plength))\
#                     .filter(plan_width__lte=1.1*float(pwidth), plan_width__gte=0.9*float(pwidth))
#         if llength and lwidth:
#             len = 0.9*llength
#             wid = 0.9*lwidth
#             lqs = qs.filter()
#===============================================================================
            
        # OR the lot and plan dimensions
        if pqs or lqs:
            queries = [pqs, lqs,]  
            qs = functools.reduce(or_, queries[1:], queries[0])  
        #=======================================================================
        # stories = request.GET.getlist('st')
        # units = request.GET.getlist('un', '')
        # rooms = request.GET.getlist('rm', '')
        # garages = request.GET.getlist('gr', '')
        #
        # qs = self.field_qs(qs, stories, 'plan_stories')
        # qs = self.field_qs(qs, units, 'plan_units')
        # qs = self.field_qs(qs, rooms, 'plan_rooms')
        # qs = self.field_qs(qs, garages, 'plan_garage')
        #=======================================================================
        return qs.order_by('id')
        
    #===========================================================================
    # def field_qs(self, qs, filter_param, field_name): # not in use!
    #     '''
    #     filter_param: a list from url parameter
    #     field_name: name of the model's field
    #     
    #     it gets a queryset and filters it with the params
    #     return: new qs
    #     '''
    #     if filter_param:
    #         if u'4' in filter_param:
    #             qs1 = qs.filter(**{field_name+'__gte':4})
    #             
    #             filter_param.remove(u'4')
    #             qs2 = qs.filter(**{field_name+'__in':filter_param})
    #             
    #             querysets = [qs1, qs2,]  
    #             qs = functools.reduce(or_, querysets[1:], querysets[0])
    #         else:
    #             qs = qs.filter(**{field_name+'__in':filter_param}) 
    # 
    #     return qs
    #===========================================================================
    
    
    
class PlanSummaryAPIview(RetrieveAPIView):
    ''' Used in order2d placement page ''' 
    #permission_classes = [?]
    queryset = Plan.objects.all()
    serializer_class = PlanSummarySerailizer
    

class FavoritePlanListAPIView(ListAPIView):
    ''' Used in the profile page 
        lists the user favorite plans
    '''
    serializer_class = FavPlanSerializer
    
    def get_serializer_context(self):
        context = {'user': self.request.user, 'request': self.request}
        return context
    
    def get_queryset(self):
        print(self.request.user)
        qs = FavPlan.objects.filter(user=self.request.user)#. 
        return qs
    
    
class AddRemoveFavoritePlanAPIView(APIView):
    '''
    used in second and third page (search results and plan details)
    add a plan to user favorite list
    '''
    serializer_class = AddRemoveFavPlanSerializer
    queryset = FavPlan.objects.all()
    
    def post(self, request, **kwargs):
        pk = kwargs['pk'] # plan_id
        user = request.user
        if FavPlan.objects.filter(user=user, plan = pk).exists():   
            # exists --> remove from favs' --> delete entry
            FavPlan.objects.filter(user=user, plan = pk).delete()
            stat = {'status': 'Plan is removed from favPlans.'}
             
        else:   # doesnt exist --> fav the plan --> create entry
            #print 'no star -> add to favs'    
            instance = FavPlan.objects.create(user=user, plan_id=pk)
            instance.createdTime = timezone.now()
            instance.save()
            stat = {'status': 'Plan is added to favPlans.'} 
        return Response(stat, status=HTTP_200_OK)
        
class ExtraPriceCreateAPIView(CreateAPIView):
    serializer_class = ExtraPriceSerializer
    queryset = ExtraPrice.objects.all()

class ExtraPriceAPIView(APIView):
    
    def get(self, format=None):
        queryset = ExtraPrice.objects.last()
        serializer = ExtraPriceSerializer(queryset)
        return Response(serializer.data)



### management views ### mass data entry ###

class FillPlanTableAPIview(APIView):
    ''' this will recieve an excel file from 
        admin and fill in the db with the contents. '''
    
    serializer_class = ExcelSerializer
    #permission_classes = [IsAdminUser,]
    queryset = Plan.objects.all()
    parser_classes = (MultiPartParser,) 
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            read_from_excel_v1(request.FILES['excelFile'])
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class FillPlanFilesAPIView(APIView):
    serializer_class = ZipSerializer
    #permission_classes = [IsAdminUser,]
    queryset = PlanFile.objects.all()
    parser_classes = (MultiPartParser,) 
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            zip_path = upload_zip_file(request.FILES['zipFile'])
            unzip_path = unzip_images(zip_path)
            fill_images(unzip_path)
            
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
        




