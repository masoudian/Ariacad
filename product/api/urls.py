from django.conf.urls import url
from django.contrib import admin

from .views import (
    AddRemoveFavoritePlanAPIView,
    ExtraPriceCreateAPIView,
    ExtraPriceAPIView,
    FavoritePlanListAPIView,
    PlanDetailAPIView,
    PlanListAPIView,
    PlanSummaryAPIview,
    
    FillPlanTableAPIview,
    FillPlanFilesAPIView,
    )

urlpatterns = [
               
    url(r'^$', PlanListAPIView.as_view(), name='list'),
    url(r'^(?P<pk>[0-9]+)/$', PlanDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/summary/$', PlanSummaryAPIview.as_view(), name='plan-summary'),
    
    url(r'^fav/$', FavoritePlanListAPIView.as_view(), name='favorite-plan-list'),
    url(r'^(?P<pk>[0-9]+)/fav/$', AddRemoveFavoritePlanAPIView.as_view(), 
        name='add-favorite-plan'), #pk is plan_id
    
    url(r'^extraPrice/$', ExtraPriceAPIView.as_view(), name='extra-price'),
    url(r'^extraPrice/new/$', ExtraPriceCreateAPIView.as_view(), name='new-extra-prices'),
    
    url(r'^fill/db/$', FillPlanTableAPIview.as_view(), name='fill-db'),
    url(r'^fill/imgs/$', FillPlanFilesAPIView.as_view(), name='fill-imgs')
]



