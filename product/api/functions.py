

from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import File
from django.core.files.storage import FileSystemStorage
from django.utils import timezone
import pandas as pd
import os
import shutil
import zipfile

from product.models import Plan, Story, UnitType, PlanClass, PlanFile


# this can read from excel file - template version 1
def read_from_excel_v1(myfile): # or url?
    duplicatelist = []
    dup_count=0
    df = pd.read_excel(myfile, sheet_name='Sheet1') 
    code_list = df["code"]
    for i in range(1, len(df.index), 2):    #rows
        #checking for duplicate codes
        if Plan.objects.filter(plan_code=code_list[i]).exists():    
            dup_count += 1
            #print (dup_count, code_list[i], ' is a duplicate---------------------------')
            duplicatelist.append(code_list[i])
            continue
        
        # checking for empty rows (end of the file)
        if not isinstance(code_list[i], str):
            break;
        
        instance = Plan(plan_code = code_list[i])
        instance.plan_width = df.iat[i,1]
        instance.plan_length = df.iat[i,2]
        instance.plan_substructure = df.iat[i,3]
        instance.plan_stories = df.iat[i,4]
        instance.plan_units = df.iat[i,5]
        instance.plan_garage = df.iat[i,16]
        
        if df.iat[i,17] == u"پیلوت":
            instance.plan_pilot = 1
        else:   # زیرزمین
            instance.plan_pilot = 0
                    
        if df.iat[i,18] == "*":
            instance.plan_pillar = 1
        else:
            instance.plan_pillar = 0
        
        if df.iat[i,19] == u"ویلایی":
            instance.plan_apartment = 0
            instance.plan_dublex = 0
        elif df.iat[i,19] == u"آپارتمانی":
            instance.plan_apartment = 1
            instance.plan_dublex = 0
        elif df.iat[i,19] == u"دوبلکس":
            instance.plan_dublex = 1
            instance.plan_apartment = 0
        else:
            print(instance.plan_code, 'error!')
            continue
        
        if df.iat[i,20] == "*":
            instance.plan_elevator = 1
        else:
            instance.plan_elevator = 0
        
        if df.iat[i,22] == "*":
            instance.plan_separateEntry = 1
        else:
            instance.plan_separateEntry = 0
        
        if pd.notnull(df.iat[i, 23]):
            instance.plan_extraFacility = df.iat[i,23]
        else:
            instance.plan_extraFacility = ""
        
        if df.iat[i,24] == "*":
            instance.plan_issued = 1
        else:
            instance.plan_issued = 0
        
        plan_class = int( df.iat[i,26]+1 )
        pclass = PlanClass.objects.get(id=plan_class)
        instance.plan_class = pclass
        
        instance.plan_krooki = df.iat[i, 27]
        instance.plan_add_date = timezone.now()
        instance.save()
        
        #PlanCover(plan=instance).save()
        
        # Unit type
        c = 0
        namelist = ['A','B','C','D','E','F','G']
        max_rooms = 0
        for j in range(12, 16):
            if pd.notnull(df.iat[i, j]):
                utype = UnitType(
                    plan = instance,
                    name = namelist[c],
                    unitSize = df.iat[i, j],
                    numberOfRooms = int(df.iat[i+1, j]),
                    )
                c += 1
                utype.save()
                if utype.numberOfRooms > max_rooms:
                    max_rooms=utype.numberOfRooms
                    
        instance.plan_rooms = max_rooms
        instance.save()
        #print('unit done')
        
        # Story
        for j in range(6, 12):  #### manfie 1 ????????????? zirzamin ??????????
            
            if  pd.notnull(df.iat[i+1, j]):
                story = Story(
                    plan = instance,
                    storyNumber = df.iat[i, j],
                    )
                unitTypes = df.iat[i+1, j].upper()
                typelist = unitTypes.split(',')
                unitnum = len(typelist)
                if 'P' in typelist:
                    unitnum -= 1
                if 'استخر' in typelist:
                    unitnum -= 1
                if 'u' in typelist:
                    unitnum -= 1
                    
                story.unitTypes = unitTypes
                story.numberOfUnits = unitnum
                
                
                story.save()
            else:
                break
        #print('story done')
        #=======================================================================
        # print(instance.plan_code, 'done')
        # print()
        #=======================================================================
    print('duplicates:', duplicatelist)
    #print('DONE!')   
    return



# uploads a zip file to 'media/temp/'
def upload_zip_file(myfile):
    fs = FileSystemStorage()
    filename = fs.save('temp/'+ myfile.name, myfile)
    uploaded_file_path = fs.path(filename)#.url(filename)
    
    return uploaded_file_path


''' recieves a zip file path 
    unzips a zip file to 'media/temp/unzipped/'   '''
def unzip_images(zip_path):
    unzip_path = "media/temp/unzipped/"
    with zipfile.ZipFile(zip_path, "r") as z:
        z.extractall(unzip_path)
    return unzip_path
    
''' recieves unzipped folder path - containing images
    saves the image's info to the planFile table and 
    
    at the end it removes the unzipped folder
    '''
def fill_images(path):
    notExistedList = []
    existedList = []
    for dirName, subdirList, fileList in os.walk(path):
        # if the directory has files in it, check if the name of it (plan_code) exists in Plan table 
        if fileList:
            folderName = dirName.split('/')[-1]    # the last subfolder's name must be in plan_code format
            try: 
                planInst =  Plan.objects.get(plan_code = folderName.upper())
                existedList.append(folderName)
                #print(folderName)
            except ObjectDoesNotExist:
                notExistedList.append(folderName)
                continue
            
        first_file = True
        for fname in sorted(fileList):
            #print('\t%s' % fname)
            fpath = os.path.join(dirName, fname) 
            if fname.endswith((".jpg", ".png", '.JPG', '.PNG')): # 'bmp' ?
                with open(fpath, 'rb') as f:
                    img_file = File(f)
                 
                    instance = PlanFile(
                        plan = planInst,
                        large_img=img_file
                    )
                    instance.save()
                     
                    # if it is the first image then add it as cover image, and no cover photo existed
                    if first_file and not planInst.cover_img:  
                        planInst.cover_img = img_file
                        planInst.save()
                        first_file = False
                     
                    img_file.close()
            else:
                print(fname, 'skipped')         # LOG IT
                
    
    shutil.rmtree(path) # deletes the unzipped folder
    #=======================================================================
    # shutil.rmtree(unzip_path)
    #=======================================================================
    print('not existed image codes!')
    print(notExistedList)      
    
    
    return
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    