#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db.models import Q
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
import django.contrib.auth.password_validation as validators
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


from userInformation.models import Profile
from product.models import Plan, PlanFile, Story, UnitType, FavPlan, ExtraPrice

from rest_framework.serializers import (
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    FileField,
    )

User = get_user_model()


class UnitTypeSerializer(ModelSerializer):
    class Meta:
        model = UnitType
        exclude = ['id']#, plan]
        
        
class StorySerializer(ModelSerializer):
    class Meta:
        model = Story
        exclude = ['id']#, plan]


class PlanFileSerializer(ModelSerializer):
    class Meta: 
        model = PlanFile
        exclude = ['id'] #, plan]


class FavouriteSerializer(ModelSerializer):
    class Meta:
        model = FavPlan
        fields = '__all__'


plan_detail_url = HyperlinkedIdentityField(
        view_name='plans-api:detail',
        #lookup_field='slug'    #default is pk
        )

class PlanDetailSerializer(ModelSerializer):
    ''' shows up in third_page '''
    url = plan_detail_url
    plan_position = SerializerMethodField()
    images = SerializerMethodField()
    unit_types = SerializerMethodField()
    stories = SerializerMethodField()
    is_fav = SerializerMethodField()
    plan_price = SerializerMethodField()
    
    class Meta:
        model = Plan
        fields = [
                  'url', 'id', 'plan_code',
                  'plan_description',
                  'plan_length', 'plan_width',
                  'plan_substructure', 'plan_position',
                  'plan_stories', 'plan_units',
                  'plan_garage',  'plan_pilot',
                  'plan_apartment', 'plan_rooms',
                  'plan_dublex', 'plan_separateEntry',
                  'plan_extraFacility', 'plan_price',
                  'plan_elevator',  'unit_types',
                  'stories', 'images', 'is_fav',
                  ]

    def get_plan_position(self, obj):
        plan_krooki = obj.get_plan_krooki_display()
        return plan_krooki
    
    def get_images(self, obj):
        img_qs = PlanFile.objects.filter(plan=obj)
        images = PlanFileSerializer(img_qs, many=True).data
        return images
    
    def get_unit_types(self, obj):
        unit_qs = UnitType.objects.filter(plan=obj)
        return UnitTypeSerializer(unit_qs, many=True).data

    def get_stories(self, obj):
        str_qs = Story.objects.filter(plan=obj)
        return StorySerializer(str_qs, many=True).data
    
    def get_is_fav(self, obj):
        if self.context['user']:
            if FavPlan.objects.filter(plan=obj, user_id=self.context['user']):
                return True
        return False
    
    def  get_plan_price(self, obj):
        return obj.plan_class.price * obj.plan_substructure  
    

class PlanListSerializer(ModelSerializer):
    url = plan_detail_url
    plan_position = SerializerMethodField()
    cover_image = SerializerMethodField()
    is_fav = SerializerMethodField()
    plan_price = SerializerMethodField()
    
    class Meta:
        model = Plan
        fields = [
                  'url', 'id', 'plan_code', 'plan_length', 'plan_width',
                  'plan_substructure', 'plan_position', 'plan_stories',
                  'plan_units', 'plan_price', 'cover_image', 'is_fav',
                  ] 
        
    def get_plan_position(self, obj):
        plan_krooki = obj.get_plan_krooki_display()
        return plan_krooki
    
    def get_cover_image(self, obj):
        file = obj.cover_img
        if obj.cover_img:
            return obj.cover_img.url
        return ''
        #return obj.plancover.cover_img.url
    
    def get_is_fav(self, obj):
        if self.context['user']:
            if FavPlan.objects.filter(plan=obj, user_id=self.context['user']):
                return True
        return False
    
    def  get_plan_price(self, obj):
        return obj.plan_class.price * obj.plan_substructure  

# this serializer shows up in the order placement page
class PlanSummarySerailizer(ModelSerializer):
    url = plan_detail_url
    plan_position = SerializerMethodField()
    cover_image = SerializerMethodField()
    plan_price = SerializerMethodField()
    
    class Meta:
        model = Plan
        fields = [
                  'url', 'id', 'plan_code',
                  'plan_length', 'plan_width',
                  'plan_substructure', 'plan_position',
                  'plan_stories', 'plan_units',
                  'plan_rooms', 'plan_price',
                  'cover_image',

                  ]
    def get_plan_position(self, obj):
        plan_krooki = obj.get_plan_krooki_display()
        return plan_krooki
    
    def get_cover_image(self, obj):
        if obj.cover_img:
            return obj.cover_img.url
        return ''
    
    def  get_plan_price(self, obj):
        return obj.plan_class.price * obj.plan_substructure
    
    
    
class FavPlanSerializer(ModelSerializer):
    plan = PlanListSerializer()
    
    class Meta:
        model = FavPlan
        fields = ['plan',]
        
        
class AddRemoveFavPlanSerializer(ModelSerializer):
    class Meta:
        model = FavPlan
        fields = '__all__'
        read_only_fields = ['user','plan', 'createdTime']
        

class ExtraPriceSerializer(ModelSerializer):
    class Meta:
        model = ExtraPrice
        fields = ['cad_price_percent', 'post_price']
        
        
from django.core.validators import FileExtensionValidator
class ExcelSerializer(Serializer):
    excelFile = FileField(validators=[FileExtensionValidator(allowed_extensions=['xlsx'])]) 
    logFile = FileField(read_only=True)
    
    
class ZipSerializer(Serializer):
    zipFile = FileField(validators=[FileExtensionValidator(allowed_extensions=['zip'])])
    logFile = FileField(read_only=True)
    
        
        
        
        
        
    
    
    