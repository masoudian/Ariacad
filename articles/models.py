from django.db import models

# Create your models here.
class Article(models.Model):
    article_type = models.SmallIntegerField(null=True)
    article_content = models.TextField(null=True)
    article_title = models.CharField(null=True, max_length=500)
    article_date = models.DateTimeField(auto_now_add=True, null=True)
    
    

class Articlefile(models.Model):
        types = ( 
         (1,'main'),
         (2,'nema'),
         (3,'boresh'),
         (4,'hayat'),
         (5,'other1'),
         (6,'other2'),
         ) 
        article = models.ForeignKey('article', on_delete=models.CASCADE)
        file_type = models. PositiveSmallIntegerField("file type", choices = types, null = True)
        article_file = models.FileField(upload_to='articlefiles/', null = True)


