#!/usr/bin/python
# -*- coding: UTF-8 -*-

from website.myfunctions import  dateToShamsi
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils import timezone
import os
#from website.storage import OverwriteStorage 

from .file_paths import *

#STATUS_CHOICES
ORDER_SAVED = 0
ORDER_PAID = 1
MODIF_FILE_0 = 2
MODIF_REQ_1 = 3
MODIF_FILE_1 = 4
MODIF_REQ_2 = 5
MODIF_FILE_2 = 6
EXTRA_MODIF_REQ = 7
EXTRA_MODIF_FILE = 8
USER_CONFIRMED = 9
FINAL_FILES_SENT = 10
ORDER_CANCELED = 11
#PAYMENT_RETURNED = 12

UserFile_extensions = ['pdf', 'jpg', 'png', ]
FinalFiles_extensions = ['zip',]
ResponseFile_extensions = ['pdf'] # or zip ?


class Order_2d(models.Model):
    plan = models.ForeignKey('product.Plan', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    order_code = models.CharField(max_length=200, null=True, default = 0)
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    state = models.CharField(max_length=100)
    planLength = models.FloatField()     #plan's dimensions
    planWidth = models.FloatField()
    lotLength = models.FloatField()      #lot's dimensions (زمین)
    lotWidth = models.FloatField()
    krooki_file = models.FileField(upload_to=krooki_file_path, null = True, 
                                   validators=[FileExtensionValidator(UserFile_extensions)] )
    
    ceiling_choices = ( 
         (1,'تخت'),
         (2,'شیبدار'),
         )
    ceiling = models.PositiveSmallIntegerField("ceiling", choices=ceiling_choices, default=1)
        
    status_choices = ( 
          (ORDER_SAVED,'ثبت سفارش'),
          (ORDER_PAID,'پرداخت انجام شد'), #a در انتظار دریافت فایل
          (MODIF_FILE_0,'فایل ارسال شد'),
          (MODIF_REQ_1,'درخواست اصلاح اول'),   #a در انتظار دریافت فایل
          (MODIF_FILE_1,'فایل اصلاح اول ارسال شد'),
          (MODIF_REQ_2,'درخواست اصلاح دوم'),  #a در انتظار دریافت فایل
          (MODIF_FILE_2,'فایل اصلاح دوم ارسال شد'),
          (EXTRA_MODIF_REQ, 'درخواست اصلاحات اضافی'),
          (EXTRA_MODIF_FILE, 'فایل اصلاحات اضافی ارسال شد'),
          (USER_CONFIRMED,'تأیید نهایی کاربر'),  #a در حال ارسال فایل‌های نهایی
          (FINAL_FILES_SENT,'تحویل نهایی'),
          #(10,'اتمام پشتیبانی'),
          (ORDER_CANCELED, 'لغو شده'),
          #(PAYMENT_RETURNED, 'عودت شده')
          #(12,'پرداخت شده')
        )
    order_status = models.PositiveSmallIntegerField("وضعیت سفارش", choices=status_choices, default=0)
    prev_status_date = models.DateTimeField(null=True) # we keep this to be able to roll back the date after a delete
    last_status_date = models.DateTimeField(null=True)
    
    #to recieve cad or postal package
    cad = models.BooleanField(default=False)
    post = models.BooleanField(default=False)
    
    total_price = models.PositiveIntegerField(default=0)    #find it from Plan 
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    final_price = models.PositiveIntegerField(default=0)
    
    user_confirmation_date = models.DateTimeField(null=True)
    
    final_response_description = models.TextField(null = True)
    final_response_files = models.FileField(upload_to=final_resp_file_path, null = True, 
                                            validators=[FileExtensionValidator(FinalFiles_extensions)])
    delivery_date = models.DateTimeField(null=True)
    
    cancelDate = models.DateTimeField(null=True)
    active = models.BooleanField(default = 0)
    
    def __str__(self):
        return str(self.id)

    def saveOrder_2d(self):
        self.save()
    
    def get_first_modif_instance(self):
        return Modification_2d.objects.filter(order=self).first()
    
    @property
    def status_date_shamsi(self):
        return dateToShamsi(self.last_status_date)
    @property
    def or_date_shamsi(self):
        return dateToShamsi(self.order_date)
    @property
    def delivery_date_shamsi(self):
        return dateToShamsi(self.delivery_date)    
    @property
    def confirmation_date_shamsi(self):
        return dateToShamsi(self.user_confirmation_date)    
    @property
    def remained_time(self):
        e1 = timezone.now() - self.last_status_date   #elapsed time from last_stat
        e1 = e1.total_seconds()
        hours = e1 / 3600     #elapsed time from last_stat in hours
        
        # we have to take action after 24 hours. check if the action time is arrived - if not order is in suspend state   
        if hours < 24:      
            return 'suspend'
        else:
            e2 = e1 - 24        #elapsed time from the start action time
            remaining_time = 48 - e2    # after 48 hours from the start, the order needs to be completed
            # if positive number --> still have time,  if negative --> deadline is failed
            
            m, s = divmod(remaining_time, 60)
            h, m = divmod(m, 60)

            remained_time = str(int(h)) + ' ساعت و ' + str(int(m)) + ' دقیقه'
            return remained_time
    @property
    def filename(self):
        return os.path.basename(self.krooki_file.name)       
##################################################################################################################

class SpecialOrder(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    order_code = models.CharField(max_length=200, null=True, default = 0)
    dimension = models.CharField(max_length=200, null=True)
    progression = models.PositiveIntegerField(default=0)
    story = models.PositiveIntegerField(default=0)
    room = models.PositiveIntegerField(default=0)
    parking = models.PositiveIntegerField(default=0)
    area_choices = ( 
         (1,'DRY'),
         (2,'HUMID'),
         (3,'COLD'),
         (4,'MILD'),
         )
    area= models.PositiveSmallIntegerField("area", choices=area_choices, default=area_choices[0][0])

    ceiling_choices = ( 
         (1,'FlAT'),
         (2,'STEEP'),
         )
    ceiling = models.PositiveSmallIntegerField("ceiling", choices=ceiling_choices, default=ceiling_choices[0][0])
    
    lighting_choices = ( 
         (1,'north'),
         (2,'south'),
         (3,'north_south'),
         (4,'do_nabsh'),
         (5,'vila'),
         )
    lighting = models.PositiveSmallIntegerField("lighting", choices=lighting_choices, default=lighting_choices[0][0])
    krooki_file = models.FileField(upload_to=krooki_file_path, null = True, 
                                   validators=[FileExtensionValidator(UserFile_extensions)])
    area_file = models.FileField(upload_to=area_sp_file_path, null = True, 
                                 validators=[FileExtensionValidator(UserFile_extensions)])
    
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    status_choices = ( 
          (ORDER_SAVED,'ثبت سفارش'),
          (ORDER_PAID,'پرداخت انجام شد'), #a در انتظار دریافت فایل
          (MODIF_FILE_0,'فایل ارسال شد'),
          (MODIF_REQ_1,'درخواست اصلاح اول'),   #a در انتظار دریافت فایل
          (MODIF_FILE_1,'فایل اصلاح اول ارسال شد'),
          (MODIF_REQ_2,'درخواست اصلاح دوم'),  #a در انتظار دریافت فایل
          (MODIF_FILE_2,'فایل اصلاح دوم ارسال شد'),
          (EXTRA_MODIF_REQ, 'درخواست اصلاحات اضافی'),
          (EXTRA_MODIF_FILE, 'فایل اصلاحات اضافی ارسال شد'),
          (USER_CONFIRMED,'تأیید نهایی کاربر'),  #a در حال ارسال فایل‌های نهایی
          (FINAL_FILES_SENT,'تحویل نهایی'),
          (ORDER_CANCELED, 'لغو شده'),
        )
    order_status = models.PositiveSmallIntegerField("وضعیت سفارش", choices=status_choices, default=0)
    prev_status_date = models.DateTimeField(null=True)
    last_status_date = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    
    cad = models.BooleanField(default=False)
    post = models.BooleanField(default=False)
    
    total_price = models.PositiveIntegerField(default=0)
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    final_price = models.PositiveIntegerField(default=0)
    
    user_confirmation_date = models.DateTimeField(null=True)
    
    final_response_description = models.TextField(null = True)
    final_response_files = models.FileField(upload_to=final_resp_file_path, null = True, 
                                            validators=[FileExtensionValidator(FinalFiles_extensions)])
    
    cancelDate = models.DateTimeField(null=True)
    active = models.BooleanField(default = 0)
    
    def saveSpecialOrder(self):
        self.save()
    
    @property
    def status_date_shamsi(self):
        return dateToShamsi(self.last_status_date)    
##################################################################################################################

class Exterior(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    order_2d = models.ForeignKey('Order_2d', null=True, related_name='exterior', on_delete=models.CASCADE)
    order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='exterior', on_delete=models.CASCADE)
    order_code = models.CharField(max_length=200, null=True, default = 0)
    materials = models.CharField(max_length=200, blank=True)
    sample_code = models.CharField(max_length=200, blank=True)
    ref_file = models.FileField("فایل رفرنس", upload_to=referenece_file_path, null=True, 
                                validators=[FileExtensionValidator(UserFile_extensions)])
    
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    status_choices = ( 
          (ORDER_SAVED,'ثبت سفارش'),
          (ORDER_PAID,'پرداخت انجام شد'), 
          (MODIF_FILE_0,'فایل ارسال شد'),
          (MODIF_REQ_1,'درخواست اصلاح اول'),  
          (MODIF_FILE_1,'فایل اصلاح اول ارسال شد'),
          (MODIF_REQ_2,'درخواست اصلاح دوم'), 
          (MODIF_FILE_2,'فایل اصلاح دوم ارسال شد'),
          (EXTRA_MODIF_REQ, 'درخواست اصلاحات اضافی'),
          (EXTRA_MODIF_FILE, 'فایل اصلاحات اضافی ارسال شد'),
          (USER_CONFIRMED,'تأیید نهایی کاربر'), 
          (FINAL_FILES_SENT,'تحویل نهایی'),
          (ORDER_CANCELED, 'لغو شده'),
          #منتظر تکمیل دوبعدی
        )
    order_status = models.PositiveSmallIntegerField("وضعیت سفارش", choices=status_choices, default=0)
    prev_status_date = models.DateTimeField(null=True)
    last_status_date = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    cad = models.BooleanField(default=False)
    post = models.BooleanField(default=False)
    total_price = models.PositiveIntegerField(default=0)
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    final_price = models.PositiveIntegerField(default=0)
    
    user_confirmation_date = models.DateTimeField(null=True)
    
    final_response_description = models.TextField(null = True)
    final_response_files = models.FileField(upload_to=final_resp_file_path, null = True, 
                                            validators=[FileExtensionValidator(FinalFiles_extensions)])
    
    cancelDate = models.DateTimeField(null=True)
    active = models.BooleanField(default = 0)
    
    def saveExterior(self):
        self.save()
    
    @property
    def status_date_shamsi(self):
        return dateToShamsi(self.last_status_date)
##################################################################################################################

class Decoration(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    order_2d = models.ForeignKey('Order_2d', null=True, related_name='decoration', on_delete=models.CASCADE)
    order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='decoration', on_delete=models.CASCADE)
    order_code = models.CharField(max_length=200, null=True, default = 0)
    sample_code = models.CharField(max_length=200, blank=True)
    ref_file = models.FileField("فایل رفرنس", upload_to=referenece_file_path, null=True, 
                                validators=[FileExtensionValidator(UserFile_extensions)])
    #description = models.TextField()
    
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    status_choices = ( 
          (ORDER_SAVED,'ثبت سفارش'),
          (ORDER_PAID,'پرداخت انجام شد'), 
          (MODIF_FILE_0,'فایل ارسال شد'),
          (MODIF_REQ_1,'درخواست اصلاح اول'),  
          (MODIF_FILE_1,'فایل اصلاح اول ارسال شد'),
          (MODIF_REQ_2,'درخواست اصلاح دوم'), 
          (MODIF_FILE_2,'فایل اصلاح دوم ارسال شد'),
          (EXTRA_MODIF_REQ, 'درخواست اصلاحات اضافی'),
          (EXTRA_MODIF_FILE, 'فایل اصلاحات اضافی ارسال شد'),
          (USER_CONFIRMED,'تأیید نهایی کاربر'), 
          (FINAL_FILES_SENT,'تحویل نهایی'),
          (ORDER_CANCELED, 'لغو شده'),
          #منتظر تکمیل دوبعدی
        )
    order_status = models.PositiveSmallIntegerField("وضعیت سفارش", choices=status_choices, default=0)
    prev_status_date = models.DateTimeField(null=True)
    last_status_date = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    
    cad = models.BooleanField(default=False)
    post = models.BooleanField(default=False)
    
    total_price = models.PositiveIntegerField(default=0)
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    final_price = models.PositiveIntegerField(default=0)
    
    user_confirmation_date = models.DateTimeField(null=True)
    
    final_response_description = models.TextField(null = True)
    final_response_files = models.FileField(upload_to=final_resp_file_path, null = True)
    
    cancelDate = models.DateTimeField(null=True)
    active = models.BooleanField(default = 0)
    
    def savedecoration(self):
        self.save()
    
    @property
    def status_date_shamsi(self):
        return dateToShamsi(self.last_status_date)
##################################################################################################################

class Roof(models.Model):   ### roof & yard -- tarahi mohavate
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    order_2d = models.ForeignKey('Order_2d', null=True, related_name='roof', on_delete=models.CASCADE)
    order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='roof', on_delete=models.CASCADE)
    order_code = models.CharField(max_length=200, null=True, default = 0)
    sample_code = models.CharField(max_length=200, blank=True)
    ref_file = models.FileField("فایل رفرنس", upload_to=referenece_file_path, null=True, 
                                validators=[FileExtensionValidator(UserFile_extensions)])
    
    order_date = models.DateTimeField(auto_now_add=True, null=True)
    status_choices = ( 
          (ORDER_SAVED,'ثبت سفارش'),
          (ORDER_PAID,'پرداخت انجام شد'), 
          (MODIF_FILE_0,'فایل ارسال شد'),
          (MODIF_REQ_1,'درخواست اصلاح اول'),  
          (MODIF_FILE_1,'فایل اصلاح اول ارسال شد'),
          (MODIF_REQ_2,'درخواست اصلاح دوم'), 
          (MODIF_FILE_2,'فایل اصلاح دوم ارسال شد'),
          (EXTRA_MODIF_REQ, 'درخواست اصلاحات اضافی'),
          (EXTRA_MODIF_FILE, 'فایل اصلاحات اضافی ارسال شد'),
          (USER_CONFIRMED,'تأیید نهایی کاربر'), 
          (FINAL_FILES_SENT,'تحویل نهایی'),
          (ORDER_CANCELED, 'لغو شده'),
          #منتظر تکمیل دوبعدی
        )
    order_status = models.PositiveSmallIntegerField(choices=status_choices, default=0)
    prev_status_date = models.DateTimeField(null=True)
    last_status_date = models.DateTimeField(null=True)
    delivery_date = models.DateTimeField(null=True)
    cad = models.BooleanField(default=False)
    post = models.BooleanField(default=False)
    total_price = models.PositiveIntegerField(default=0)
    discount = models.DecimalField(max_digits=4, decimal_places=2, default=0)
    final_price = models.PositiveIntegerField(default=0)
    
    user_confirmation_date = models.DateTimeField(null=True)
    
    final_response_description = models.TextField(null = True)
    final_response_files = models.FileField(upload_to=final_resp_file_path, null = True, 
                                            validators=[FileExtensionValidator(FinalFiles_extensions)])
    
    cancelDate = models.DateTimeField(null=True)
    active = models.BooleanField(default = 0)
    
    def saveRoof(self):
        self.save()
    
    @property
    def status_date_shamsi(self):
        return dateToShamsi(self.last_status_date)
##################################################################################################################
''' with this model 3d orders can be related
'''
class Related3D(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, default=0)
    exterior = models.ForeignKey('Exterior', on_delete=models.CASCADE)
    decoration = models.ForeignKey('Decoration', on_delete=models.CASCADE)
    roof = models.ForeignKey('Roof', on_delete=models.CASCADE)
    #landscape = models.ForeignKey('Landscape', on_delete=models.CASCADE)   
    
##################################################################################################################
# remove it??  
class BoughtPlan(models.Model):
    
    plan = models.ForeignKey('product.Plan', on_delete=models.CASCADE)
    
    #order = models.ForeignKey('Order', on_delete=models.CASCADE)
    
    def saveboughtPlan(self):
        self.save()
##################################################################################################################
''' اصلاحیات 
    شماره از 0 تا 2
    صفر هنگام ثبت سفارش ذخیره میشود
'''
class Modification_2d(models.Model):       ##previous LateModification 
    order = models.ForeignKey('Order_2d', null=True, related_name='modification', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    active = models.BooleanField(default = 1)   # extra modifs will be 0 before the payment
    modification_number = models.IntegerField(default=0)    
    req_description = models.TextField()
    req_date = models.DateTimeField()
    response_description = models.TextField(null = True)
    response_file = models.FileField(upload_to=response_file_path, null = True, 
                                     validators=[FileExtensionValidator(ResponseFile_extensions)])
    response_date = models.DateTimeField(null=True)
    
    class Meta:         # order_by id
        ordering = ['id']
    
    def saveModification(self):
        self.save()
    
    def __str__(self):
        return str(self.id)
    
    @property
    def req_date_shamsi(self):
        return dateToShamsi(self.req_date)
    @property
    def resp_date_shamsi(self):
        return dateToShamsi(self.response_date)
##################################################################################################################

class Modification_SP(models.Model):
    order = models.ForeignKey('SpecialOrder', null=True, related_name='modification', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    active = models.BooleanField(default = 1)
    modification_number = models.IntegerField(default=0)    
    req_description = models.TextField()
    req_date = models.DateTimeField()
    response_description = models.TextField(blank = True)
    response_file = models.FileField(upload_to=response_file_path, null = True, 
                                     validators=[FileExtensionValidator(ResponseFile_extensions)])
    response_date = models.DateTimeField(null=True)
    
    def saveModification(self):
        self.save()
        
    @property
    def req_date_shamsi(self):
        return dateToShamsi(self.req_date)
    
    @property
    def resp_date_shamsi(self):
        return dateToShamsi(self.response_date)
##################################################################################################################

class Modification_EX(models.Model):
    order = models.ForeignKey('Exterior', null=True, related_name='modification', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    active = models.BooleanField(default = 1)
    modification_number = models.IntegerField(default=0)        
    req_description = models.TextField()
    req_date = models.DateTimeField()
    response_description = models.TextField(blank = True)
    response_file = models.FileField(upload_to=response_file_path, null = True, 
                                     validators=[FileExtensionValidator(ResponseFile_extensions)])
    response_date = models.DateTimeField(null=True)
    
    def saveModification(self):
        self.save()
        
    @property
    def req_date_shamsi(self):
        return dateToShamsi(self.req_date)
    
    @property
    def resp_date_shamsi(self):
        return dateToShamsi(self.response_date)
##################################################################################################################

class Modification_DEC(models.Model):
    order = models.ForeignKey('Decoration', null=True, related_name='modification', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    active = models.BooleanField(default = 1)
    modification_number = models.IntegerField(default=0)    
    req_description = models.TextField()
    req_date = models.DateTimeField()
    response_description = models.TextField(blank = True)
    response_file = models.FileField(upload_to=response_file_path, null = True, 
                                     validators=[FileExtensionValidator(ResponseFile_extensions)])
    response_date = models.DateTimeField(null=True)
    
    def saveModification(self):
        self.save()
        
    @property
    def req_date_shamsi(self):
        return dateToShamsi(self.req_date)
    
    @property
    def resp_date_shamsi(self):
        return dateToShamsi(self.response_date)
##################################################################################################################

class Modification_RF(models.Model):
    order = models.ForeignKey('Roof', null=True, related_name='Modification_RF', on_delete=models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    active = models.BooleanField(default = 1)
    modification_number = models.IntegerField(default=0)    
    req_description = models.TextField()
    req_date = models.DateTimeField()
    response_description = models.TextField(blank = True)
    response_file = models.FileField(upload_to=response_file_path, null = True, 
                                     validators=[FileExtensionValidator(ResponseFile_extensions)])
    response_date = models.DateTimeField(null=True)
    
    def saveModification(self):
        self.save()
        
    @property
    def req_date_shamsi(self):
        return dateToShamsi(self.req_date)
    
    @property
    def resp_date_shamsi(self):
        return dateToShamsi(self.response_date)
##################################################################################################################

class Payment(models.Model):
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, default=0)
    order_2d = models.ForeignKey('Order_2d', null=True, related_name='payment', on_delete=models.CASCADE, blank=True)
    order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='payment', on_delete=models.CASCADE, blank=True)
    order_ex = models.ForeignKey('Exterior', null=True, related_name='payment', on_delete=models.CASCADE, blank=True)
    order_dec = models.ForeignKey('Decoration', null=True, related_name='payment', on_delete=models.CASCADE, blank=True)
    order_rf = models.ForeignKey('Roof', null=True, related_name='payment', on_delete=models.CASCADE, blank=True)
    
    payment_date = models.DateTimeField()
    payment_amount = models.FloatField(default=0)
    
    payment_method = models.CharField(max_length=100, blank=True)   #??
    
    def savePayment(self):
        print('here')
        if self.order_2d:
            print('here')
            self.order_2d.order_status = ORDER_PAID
        elif self.order_sp:
            self.order_sp.order_status = ORDER_PAID
        self.save()
    
    @property
    def pay_date_shamsi(self):
        return dateToShamsi(self.payment_date)
##################################################################################################################
        
class Gaurantee(models.Model):      #maybe change
    
    #===========================================================================
    # order_2d = models.ForeignKey('Order_2d', null=True, related_name='modification', on_delete=models.CASCADE)
    # order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='modification', on_delete=models.CASCADE)
    # order_ex = models.ForeignKey('Exterior', null=True, related_name='modification', on_delete=models.CASCADE)
    # order_dec = models.ForeignKey('Decoration', null=True, related_name='modification', on_delete=models.CASCADE)
    # order_rf = models.ForeignKey('Roof', null=True, related_name='modification', on_delete=models.CASCADE)
    #===========================================================================
    types = (
          (1,'2_bodi'),
          (2,'special'),
          (3,'exterior'),
          (4,'area'),
          (5,'roof'),
             )
    order_type = models.PositiveSmallIntegerField("نوع سفارش", choices=types, default=0)
    
    gaurantee_type = models.CharField(max_length=100)   #??
    gaurantee_status = models.CharField(max_length=100) #??
    
    gaurantee_StartDate = models.DateTimeField()
    gaurantee_EndDate = models.DateTimeField()
    
    def saveGaurantee(self):
        self.save()
##################################################################################################################
class ReturnPayment(models.Model):     
    
    order_2d = models.ForeignKey('Order_2d', null=True, related_name='retPayment', on_delete=models.CASCADE)
    order_sp = models.ForeignKey('SpecialOrder', null=True, related_name='retPayment', on_delete=models.CASCADE)
    order_ex = models.ForeignKey('Exterior', null=True, related_name='retPayment', on_delete=models.CASCADE)
    order_dec = models.ForeignKey('Decoration', null=True, related_name='retPayment', on_delete=models.CASCADE)
    order_rf = models.ForeignKey('Roof', null=True, related_name='retPayment', on_delete=models.CASCADE)

    
    payment = models.ForeignKey('payment', on_delete=models.CASCADE)
    
    card_number = models.CharField(max_length=20, blank=True)
    card_ownername = models.CharField(max_length=100, blank=True)
    
    reason = models.TextField()
    cancel_date = models.DateTimeField(default = timezone.now)
    
    return_amount = models.FloatField()
    return_date = models.DateTimeField(null=True)
    
    def saveReturnPayment(self):
        self.save()
    
    @property
    def return_date_shamsi(self):
        return dateToShamsi(self.return_date)
    
    @property
    def cancel_date_shamsi(self):
        return dateToShamsi(self.cancel_date)
    
    
##################################################################################################################

# remove it??
class Fromarchive(models.Model):
    #order = models.ForeignKey('Order', on_delete=models.CASCADE)
    plan = models.ForeignKey('product.Plan', on_delete=models.CASCADE)
    order_name = models.CharField(max_length=100, null=True)

    def saveArchiveOrder(self):
        self.save()

