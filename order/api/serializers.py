#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
from django.db.models import Q
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
import django.contrib.auth.password_validation as validators
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from product.api.serializers import PlanDetailSerializer
from order import models
from order.models import (
                    Order_2d,
                    SpecialOrder,
                    Roof,
                    Decoration,
                    Exterior,
                    Modification_2d, 
                    Modification_SP,
                    Modification_EX,
                    Modification_DEC,
                    Modification_RF,
                    Payment,
                    ReturnPayment,
                    )
from product.models import Plan
from website.myfunctions import generateOrderCode
 
from rest_framework.reverse import reverse
from rest_framework.serializers import (
    CharField,
    HyperlinkedIdentityField,
    HyperlinkedModelSerializer,
    ListSerializer,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    ) 
 
 
User = get_user_model()


class SpecialOrderSummarySerializer(ModelSerializer):
    class Meta:
        model = SpecialOrder
        fields = '__all__'
             
class RoofOrderSummarySerializer(ModelSerializer):
    class Meta:
        model = Roof
        #fields = '__all__'
        exclude = ('user', 'order_2d', 'order_sp')
 
class DecorationOrderSummarySerializer(ModelSerializer):
    class Meta:
        model = Decoration
        exclude = ('user', 'order_2d', 'order_sp')
 
class ExteriorOrderSummarySerializer(ModelSerializer):
    class Meta:
        model = Exterior
        exclude = ('user', 'order_2d', 'order_sp')


class Order2dDescriptionSerializer(ModelSerializer):
    class Meta:
        model = Modification_2d
        fields = ['id', 'req_description', ]
    
    def create(self, validated_data):
        order_2d = self.context['order_2d']
        user = self.context['user']
        modif_count = Modification_2d.objects.filter(order=order_2d).count()
        new_modif = Modification_2d.objects.create(order=order_2d, user = user,
                                                  req_description=validated_data['req_description'],
                                                  req_date=timezone.now(),
                                                  modification_number = modif_count
                                                  )
        order = new_modif.order
        if modif_count > 3:     # is an extra request
            order.order_status = models.EXTRA_MODIF_REQ
            new_modif.active = 0    # after payment it will change to 1
        else:
            order.order_status += 1
            
        order.prev_status_date = order.last_status_date
        order.last_status_date = new_modif.req_date
        order.save()
        
        return new_modif

    def update(self, instance, validated_data):
        instance = super(Order2dDescriptionSerializer, self).update(instance, validated_data)
        #instance.req_date = timezone.now()
        instance.save()
        return instance
        


class Order2dAddResponseSerializer(ModelSerializer):
    class Meta:
        model = Modification_2d
        fields = ['response_description', 'response_file',] # '__all__'#
        
    def update(self, instance, validated_data):
        if not validated_data['response_file']:
            validated_data.pop('response_file')
        
        instance = super(Order2dAddResponseSerializer, self).update(instance, validated_data)
        instance.response_date = timezone.now()
        instance.save()
                
        # updating an existing response won't change status
        order = instance.order
        stat = instance.order.order_status
        if(stat == models.ORDER_PAID or stat == models.MODIF_REQ_1 or
           stat == models.MODIF_REQ_2 or stat == models.EXTRA_MODIF_REQ ):
            order.order_status = stat + 1
            order.prev_status_date = order.last_status_date
            order.last_status_date = instance.response_date
            order.save()
        
        return instance

class Order2dEditResponseSerializer(ModelSerializer):
    class Meta:
        model = Modification_2d
        fields = ['response_description', 'response_file',]

    

class Order2dPlacementSerializer(ModelSerializer):
    ceiling = SerializerMethodField()
    modification = Order2dDescriptionSerializer(source="get_first_modif_instance")
    class Meta:
        model = Order_2d
        fields = [
                  'id',
                  'plan',
                  'order_code',
                  'state',
                  'planLength',
                  'planWidth',
                  'lotLength',
                  'lotWidth',
                  'ceiling',
                  'krooki_file',
                  'cad',
                  'post',
                  'modification',
                  ]
        read_only_fields = ('order', 'order_code', 'plan')
     
    def get_ceiling(self, obj):
        return obj.get_ceiling_display()
     
    def create(self, validated_data):
        user = self.context.get('user', None)
        plan_id = self.context.get('plan_id')
        plan = Plan.objects.get(pk=plan_id)
        description = validated_data.pop('get_first_modif_instance', {})
        
        new_2d = Order_2d.objects.create(plan=plan, user=user, **validated_data)
        new_2d.plan_id = plan.id
        new_2d.order_date = timezone.now()
        #----------------------------------------------------------------------------------------------
        ''' total price has to be calculated: plan_price + (cad + post) if selected ??? '''
        new_2d.total_price = plan.plan_class.price
        #new_2d.discount = 
        #new_2d.final_price = 
        #new_2d.prepayment = plan.plan_class.price * PrepaymentAmount.percents
        #----------------------------------------------------------------------------------------------
        new_2d.last_status_date = new_2d.order_date #تاریخ الان برای وضعیت "ثبت سفارش"  ب
        new_2d.prev_status_date = new_2d.last_status_date
        new_2d.order_code = "2d-" + generateOrderCode(new_2d.id)
        new_2d.save()
         
        new_modification = Modification_2d(user=user, order=new_2d)
        new_modification.req_description = description['req_description']
        new_modification.req_date = new_2d.order_date
        new_modification.save()
         
        return new_2d
     
    def update(self, instance, validated_data):
        #description = self.validated_data.pop('modification', {})
        description = validated_data.pop('get_first_modif_instance', {})
        
        if not validated_data['krooki_file']:       # if the field is empty do not update it!
            validated_data.pop('krooki_file')
        
        instance = super(Order2dPlacementSerializer, self).update(instance, validated_data)
        instance.order_date = timezone.now()
        
        #----------------------------------------------------------------------------------------------
        ''' total price has to be calculated: plan_price + (cad + post) if selected ??? '''
        instance.total_price = instance.plan.plan_class.price
        #new_2d.discount = 
        #new_2d.final_price = 
        #new_2d.prepayment = plan.plan_class.price * PrepaymentAmount.percents
        #----------------------------------------------------------------------------------------------
        
        instance.last_status_date = instance.order_date
        instance.save()
         
        modif = Modification_2d.objects.get(order=instance)
        modif.req_description = description['req_description'] # self.request['description_w']
        modif.req_date = instance.order_date
        modif.save()
 
        return instance


class Order2dEditSerializer(ModelSerializer): 
    ''' get_first_modif_instance is a function defined in Order_2d model, 
        in the json file created by PUT, it oddly changes the item name (modification) 
        to the function name (get_first_modif_instance) - see update method below! '''
    modification = Order2dDescriptionSerializer(source="get_first_modif_instance")
    plan_url = SerializerMethodField()  
    class Meta:
        model = Order_2d
        fields = ['id',
                  'plan_url',
                  'order_code',
                  'state',
                  'planLength',
                  'planWidth',
                  'lotLength',
                  'lotWidth',
                  'ceiling',
                  'krooki_file',
                  'cad',
                  'post',
                  'modification',
                  ]
        read_only_fields = ('order',  'order_code')
       
    def get_plan_url(self, obj):        
        request = self.context['request']
        return reverse('plans-api:detail', args=[obj.plan.id], request=request)
      
    def update(self, instance, validated_data):
        #modif = validated_data.pop('modification', {})
        description = validated_data.pop('get_first_modif_instance', {})
        
        if not validated_data['krooki_file']:       # if the field is empty do not update it!
            validated_data.pop('krooki_file')
        
        modif_instance = Modification_2d.objects.filter(order=instance).first()
        modif_instance.req_description = description['req_description']
        modif_instance.save()
          
        instance = super(Order2dEditSerializer, self).update(instance, validated_data)
        return instance


class Order2dListSerializer(ModelSerializer):
    roof = RoofOrderSummarySerializer(many=True) 
    decoration = DecorationOrderSummarySerializer(many=True) 
    exterior = ExteriorOrderSummarySerializer(many=True)
    
    status = SerializerMethodField()
    url = HyperlinkedIdentityField(view_name='orders-api:order2d-detail')
    class Meta:
        model = Order_2d
        fields = [
                  'id',
                  'user',
                  'url',
                  'order_code',
                  'order_date',
                  'status',
                  'last_status_date',
                  'total_price',
                  'discount',
                  'final_price',
                  'roof',
                  'decoration',
                  'exterior',
                 # garantee stat??
                  ]
    def get_status(self, obj):
        return obj.get_order_status_display()


''' retrieves details of modifications req & resp '''
class Modification_2dDetailSerializer(ModelSerializer):
    response_file = SerializerMethodField()
    class Meta:
        model = Modification_2d
        fields = [
                  'id',
                  'modification_number',
                  'req_description',
                  'req_date',
                  'response_description',
                  'response_file',
                  'response_date',
                  ]
    def get_response_file(self, obj):
        file = obj.response_file
        if file:
            return file.url
        return ''


class Order2dDetailSerializer(ModelSerializer):
    ceiling = SerializerMethodField()
    krooki_file = SerializerMethodField()
    order_status = SerializerMethodField()
    final_response_files = SerializerMethodField()
    plan_url = SerializerMethodField()
    modification = Modification_2dDetailSerializer(many=True)
       
    class Meta:
        model = Order_2d
        exclude = ('active',)
       
    def get_ceiling(self, obj):
        return obj.get_ceiling_display()
       
    def get_krooki_file(self, obj):
        file = obj.krooki_file
        if file:
            return file.url
        return ''
       
    def get_order_status(self, obj):
        return obj.get_order_status_display()
       
    def get_final_response_files(self, obj):
        file = obj.final_response_files
        if file:
            return file.url
        return ''
       
    def get_plan_url(self, obj):        
        request = self.context['request']
        return reverse('plans-api:detail', args=[obj.plan.id], request=request)


class SavedOrder2dListSerializer(ModelSerializer):
    plan_code = SerializerMethodField() 
    #url = HyperlinkedIdentityField(view_name='orders-api:order2d-placement', source=)
    class Meta:
        model = Order_2d
        fields = [
            'id',
            'order_code',
            'order_date',
            'plan_code',
            ]
    def get_plan_code(self, obj):
        return obj.plan.plan_code


class Order2dUserConfirmation(ModelSerializer):
    order_status = SerializerMethodField()
    class Meta: 
        model = Order_2d
        fields = ['order_status', 'last_status_date', 'user_confirmation_date', ]
        read_only_fields =  ['order_status', 'last_status_date', 'user_confirmation_date', ]
       
    def get_order_status(self, obj):
        return obj.get_order_status_display()
       
    def update(self, instance, validated_data):
        instance.order_status = models.USER_CONFIRMED
        instance.prev_status_date = instance.last_status_date
        instance.last_status_date = timezone.now()
        instance.user_confirmation_date = instance.last_status_date
        instance.save()
        return instance


class PaymentSerializer(ModelSerializer):
    class Meta:
        model = Payment
        #fields = '__all__'
        exclude = ('order_2d', 'order_sp', 'order_ex', 'order_dec', 'order_rf')

#===============================================================================
# ''' this serializer works for all of the suborder types
# '''
# class ReqDescriptionSerializer(ModelSerializer):
#     class Meta:
#         model = Modification
#         fields = ['req_description', ]
#       
#     def create(self, validated_data):
#         suborder = self.context['suborder']
#           
#         if isinstance(suborder, Order_2d):
#             return Modification.objects.create(order_2d=suborder, 
#                                         order=suborder.order, 
#                                         req_description=validated_data['req_description'],
#                                         req_date=timezone.now()
#                                         )
#         elif isinstance(suborder, SpecialOrder):
#             return Modification.objects.create(order_sp=suborder, 
#                                         order=suborder.order, 
#                                         req_description=validated_data['req_description'],
#                                         req_date=timezone.now()
#                                         )
#         elif isinstance(suborder, Exterior):
#             return Modification.objects.create(order_ex=suborder, 
#                                         order=suborder.order, 
#                                         req_description=validated_data['req_description'],
#                                         req_date=timezone.now()
#                                         )
#         elif isinstance(suborder, Decoration):
#             return Modification.objects.create(order_dec=suborder, 
#                                         order=suborder.order, 
#                                         req_description=validated_data['req_description'],
#                                         req_date=timezone.now()
#                                         )
#           
#         else: # is roof
#             return Modification.objects.create(order_rf=suborder, 
#                                         order=suborder.order, 
#                                         req_description=validated_data['req_description'],
#                                         req_date=timezone.now()
#                                         )
#         # is this line nessesory???
#         return ModelSerializer.create(self, validated_data)
#       
#     def update(self, instance, validated_data):
#         instance = super(ReqDescriptionSerializer, self).update(instance, validated_data)
#         instance.req_date = timezone.now()
#         instance.save()
#         return instance
#
# #===============================================================================
# # class FilterActiveOrderSerializer(ListSerializer):
# #     def to_representation(self, data):
# #         data = data.filter(order_type=1)
# #         return super(FilterActiveOrderSerializer, self).to_representation(data)
# #         #list_serializer_class = FilterActiveOrderSerializer  # add this to target serializer in class meta
# #===============================================================================
#   
# class ReturnPaymentSerailizer(ModelSerializer):
#     class Meta:
#         model = ReturnPayment
#         fields = '__all__'
#===============================================================================
