#!/usr/bin/python
# -*- coding: UTF-8 -*-
from django.contrib.auth import get_user_model
from django.db.models import Prefetch
from django.http import Http404
from django.shortcuts import get_object_or_404, _get_queryset

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND, HTTP_204_NO_CONTENT, HTTP_403_FORBIDDEN
from rest_framework.views import APIView

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView, 
    RetrieveUpdateAPIView,
    RetrieveUpdateDestroyAPIView,
    ListAPIView,
    UpdateAPIView,
    
    )
#from rest_framework.decorators import parser_classes
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAdminUser

User = get_user_model()
from product.models import Plan
from order.models import (
                    Order_2d,
                    SpecialOrder,
                    Roof,
                    Decoration,
                    Exterior,
                    Modification_2d, 
                    Modification_SP,
                    Modification_EX,
                    Modification_DEC,
                    Modification_RF,
                    Payment,
                    ReturnPayment,
                    ) 

from product.api.serializers import PlanSummarySerailizer
from .permissions import (
                    IsOwner,
                    IsOwnerOrAdminUser,
                    IsCancelabe,
                    IsEditableOrder, 
                    InFileSentState,
                    IsModifEditable,
                    IsNotWaiting,
                    IsActive,
                    )

from .serializers import ( 
    
    Order2dPlacementSerializer,
    Order2dEditSerializer,
    Order2dListSerializer,
    Order2dDetailSerializer,
    SavedOrder2dListSerializer,
    Order2dDescriptionSerializer,
    Order2dAddResponseSerializer,
    Order2dUserConfirmation,
    
    PaymentSerializer,
    # ReturnPaymentSerailizer,  or ? ReturnPaymentSerializer  ?? or remove it!!
    
    )


''' THIS STEP IS BEFORE USER PAYMENT SO IT WILL BE active=0 UNTILL THE PAYMENT
    save a new order for a specific plan and user,
    or update/delete existing order,
    permissions: is_authenticated
'''
class PlaceOrder2dAPIView(APIView):
    '''id is plan_id'''
    serializer_class = Order2dPlacementSerializer
    queryset = Order_2d.objects.all()
    parser_classes = (MultiPartParser,)   
    
    def get_object(self, pk):
        try:
            return Order_2d.objects.get(plan=pk, user=self.request.user, active=False)
        except Order_2d.DoesNotExist:
            raise Http404
    
    def get(self, request, pk, format=None):
        try:
            or_2d = Order_2d.objects.get(plan=pk, user=self.request.user, active=False)
            serializer = self.serializer_class(or_2d)
            return Response(serializer.data)
        except Order_2d.DoesNotExist:
            return Response('')
        #or_2d = self.get_object(pk)
        #serializer = self.serializer_class(or_2d)
        #return Response(serializer.data)
        
    def put(self, request, pk, format=None):
        or_2d = self.get_object(pk)
        serializer = self.serializer_class(or_2d, data=request.data)
        if serializer.is_valid():
            serializer.save(raise_exception=True)
            return Response(serializer.data)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)    
    
    def post(self, request, pk):
        plan_id = pk
        get_object_or_404(Plan, pk=plan_id) # this is to restrict if the plan not exists
        
        try:                                # if an instance already exists, raise 403. Else post it.
            Order_2d.objects.get(plan=pk, user=self.request.user, active=False)
            return Response(status=HTTP_403_FORBIDDEN)
        
        except Order_2d.DoesNotExist:       # otherwise create
            context = {'plan_id':plan_id, 'user':self.request.user}
            serializer = self.serializer_class(data=request.data, context=context)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
                return Response(serializer.data, status=HTTP_200_OK)
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk, format=None):
        or_2d = self.get_object(pk)
        or_2d.delete()
        return Response({"detail": "Successfully deleted the saved order."},status=HTTP_204_NO_CONTENT)


''' order_2d summary
    this will retrieve active 2d orders, with their active 3d suborders
    permissions: is_auth 
    نمایش در داشبورد کاربر - سفارشهای من 
'''
class Order2dListAPIView(ListAPIView):
    ''' this will only retrieve active orders (پرداخت انجام شده باشد) '''
    serializer_class = Order2dListSerializer
    def get_queryset(self): 
        qs = ( Order_2d.objects.filter(user=self.request.user, active=1)
                                .prefetch_related (
                                    Prefetch('roof', Roof.objects.filter(active=1)),
                                    Prefetch('decoration', Decoration.objects.filter(active=1)),
                                    Prefetch('exterior', Exterior.objects.filter(active=1)),  
                                )
              )
        return qs     

''' for third page '''
class Order2dDetailAPIView(RetrieveAPIView):    # modification list can be separated maybe???
    serializer_class = Order2dDetailSerializer
    queryset = Order_2d.objects.all()
    permission_classes = [IsOwnerOrAdminUser, IsActive]
    
''' for third page '''
class PaymentDetailRetriveAPIView(RetrieveAPIView):
    #===========================================================================
    # serializer_class = PaymentSerializer
    # queryset = Payment.objects.all()
    # permission_classes = [IsOwnerOrAdminUser,]
    #===========================================================================
    #===========================================================================
    # def get_object(self, pk):
    #     return Payment.objects.filter(order_)    شاید مدل اینوویس لازم باشه؟؟
    # یا اینکه برای هر نوع سفارش یک ویوی جدا بنویسم؟؟
    #===========================================================================
    pass


''' for third page '''
class GaranteeRetrieveAPIView(RetrieveAPIView):
    pass
    
''' Edit an active (payed) order2d
    permissions: is_owner, is_before24h  '''
class Order2dEditAPIView(RetrieveUpdateAPIView):
    serializer_class = Order2dEditSerializer
    queryset = Order_2d.objects.filter(active=1)
    permission_classes = [IsOwner, IsEditableOrder]
    parser_classes = (MultiPartParser,) 


class SavedOrder2dListAPIView(ListAPIView):
    ''' this will show saved orders for the current user
    '''
    serializer_class = SavedOrder2dListSerializer
    queryset = Order_2d.objects.filter(active=0)
    permission_classes = [IsOwnerOrAdminUser,]
    

''' permissions: is_owner, order_status=2or4 '''
class Modification_2dCreateAPIView(CreateAPIView):
    ''' creates a new modification request for order_2d, pk=order_2d.id '''
    serializer_class = Order2dDescriptionSerializer
    permission_classes = [IsOwner, InFileSentState]
    
    def post(self, request, pk):
        or2d = get_object_or_404(Order_2d, pk=pk)
        self.check_object_permissions(self.request, or2d)
        
        serializer = Order2dDescriptionSerializer(data=request.data, context = {'order_2d':or2d, 'user':request.user})
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            # if we add serializer.data, we get an error on permission! (permission will run with modif_obj)
            return Response(status=HTTP_200_OK) 
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


''' edit and delete the modification entry
    works for all order types
    gets pk=modif_id, 
    permissions: is_owner, elapsed_time<threshold  (24h)
'''
class Modification_2dUpdateDeleteAPIView(RetrieveUpdateDestroyAPIView):
    ''' Edit/delete a modification request '''
    queryset = Modification_2d.objects.all()
    serializer_class = Order2dDescriptionSerializer
    permission_classes = [IsOwner, IsModifEditable]
    
    def delete(self, request, *args, **kwargs):
        pk = kwargs['pk']
        modif = Modification_2d.objects.get(pk=pk)
        order = modif.order
        order.order_status -= 1
        order.last_status_date = order.prev_status_date
        order.save()
        
        modif.delete()
  

class Response_2dUpdateAPIView(RetrieveUpdateAPIView):
    ''' adds/updates a response to a modification request 
        adding a response is allowed 24h after modif request '''
    queryset = Modification_2d.objects.all()
    serializer_class = Order2dAddResponseSerializer
    permission_classes = [IsAdminUser, IsNotWaiting]


''' user final confirmation 
    permissions: is_owner, is_confirmable(status=2or4or6)
'''
class Order2dUserConfirmationAPIView(RetrieveUpdateAPIView):
    queryset = Order_2d.objects.all()
    serializer_class = Order2dUserConfirmation
    permission_classes = [IsOwner, InFileSentState]
    

class Order2dCencelAPIView(RetrieveUpdateAPIView):
    #permission_classes = [IsOwner, IsEditableOrder]
    pass



class PackagePostRequest():
    pass


