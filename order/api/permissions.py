#!/usr/bin/python
# -*- coding: UTF-8 -*-

from rest_framework.permissions import BasePermission, SAFE_METHODS
from website.myfunctions import calculate_time_elapsed
from order.models import Order_2d
from order import models
from django.utils import timezone

class IsOwner(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
    

class IsOwnerOrAdminUser(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user or request.user.is_staff

class IsActive(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.active == True


''' obj is an Order object
    An order can be canceled if all suborders are editable
    (24 hours after order's created time) '''
class IsCancelabe(BasePermission):
    def has_object_permission(self, request, view, obj):
        elapsed_time = calculate_time_elapsed(obj.created, 1)
        return elapsed_time < 24

''' can be used for all of the order types
    This will check if an order can be "EDITed or CANCELed"
    A suborders is editable 24 hours after its placement '''
class IsEditableOrder(BasePermission):
    def has_object_permission(self, request, view, obj):
        elapsed_time = calculate_time_elapsed(obj.order_date, 1)
        return elapsed_time < 24 and obj.cancelDate is None
    
    
''' obj is a modif object
    is in waiting state '''
class IsModifEditable(BasePermission):
    def has_object_permission(self, request, view, obj):
        elapsed_time = calculate_time_elapsed(obj.req_date, 1)
        return elapsed_time < 24

''' it checks if modif request is not in waiting state: 
    (24 hours is passed from the request) '''
class IsNotWaiting(BasePermission):
    def has_object_permission(self, request, view, obj):
        elapsed_time = calculate_time_elapsed(obj.req_date, 1)
        return elapsed_time > 24


''' obj is a order object (can be used for all of the types) 
    if is in this state then: 
        - a new modification request is allowed
        - user confirmation is allowed
'''
class InFileSentState(BasePermission):
    def has_object_permission(self, request, view, obj):
        return( obj.order_status==models.MODIF_FILE_0 or 
                obj.order_status==models.MODIF_FILE_1 or 
                obj.order_status==models.MODIF_FILE_2 or
                obj.order_status==models.EXTRA_MODIF_FILE
                )







    