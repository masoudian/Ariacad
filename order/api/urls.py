from django.conf.urls import url

from .views import (
    
    Order2dEditAPIView,
    Order2dListAPIView,
    Order2dDetailAPIView,
    PlaceOrder2dAPIView,
    SavedOrder2dListAPIView,
    Order2dUserConfirmationAPIView,
    
    Modification_2dCreateAPIView,
    Modification_2dUpdateDeleteAPIView,
    Response_2dUpdateAPIView,
    
    )

urlpatterns = [
    
    url(r'^2d/new/(?P<pk>[0-9]+)/$', PlaceOrder2dAPIView.as_view(), name='order2d-placement'), #pk=plan_id
    
    url(r'^2d/$', Order2dListAPIView.as_view(), name='order2d-list'),
    url(r'^2d/(?P<pk>[0-9]+)/$', Order2dDetailAPIView.as_view(), name='order2d-detail'),
    url(r'^2d/(?P<pk>[0-9]+)/edit/$', Order2dEditAPIView.as_view(), name='order2d-edit'),
    url(r'^2d/savedlist/$', SavedOrder2dListAPIView.as_view(), name='saved2d-list'),
    url(r'^2d/(?P<pk>[0-9]+)/confirm/$', Order2dUserConfirmationAPIView.as_view(), name='order2d-confirm'),
    
    url(r'^2d/modif/new/(?P<pk>[0-9]+)/$', Modification_2dCreateAPIView.as_view(), name='modif2d-new'), # pk=order2d_id
    url(r'^2d/modif/(?P<pk>[0-9]+)/edit/', Modification_2dUpdateDeleteAPIView.as_view(), name='modif2d-edit'), #pk=modif_id-edit/delete
    url(r'^2d/modif/(?P<pk>[0-9]+)/response/', Response_2dUpdateAPIView.as_view(), name='modif2d-response'), #pk=modif_id
    
    
    #url(r'^2d/(?P<pk>[0-9]+)/cancel/$', CencelOrder2d.as_view(), name='order2d-cancel'),
    
]



