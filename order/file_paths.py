

def krooki_file_path(instance, filename):
    extension = filename.split('.')[-1]
    fname = str(instance.order_code) + "-krooki." + extension
    return '/'.join([ 'krookiFiles', fname ])

def area_sp_file_path(instance, filename):
    extension = filename.split('.')[-1]
    fname = str(instance.order_code) + "-Area." + extension
    return '/'.join([ 'krookiFiles', fname ])

def referenece_file_path(instance, filename):
    extension = filename.split('.')[-1]
    fname = str(instance.order_code) + "-ref." + extension
    return '/'.join([ 'ReferenceFiles', fname ])
    

def response_file_path(instance, filename):
    extension = filename.split('.')[-1]
    fname = str(instance.order.order_code) + '-response' + str(instance.modification_number+1) + "." + extension
    return '/'.join([ 'Modifications', fname ])

def final_resp_file_path(instance, filename):   # zip file
    extension = filename.split('.')[-1]
    fname = str(instance.order_code) + '-FinalFiles' + "." + extension
    return '/'.join([ 'FinalFiles', fname ])

