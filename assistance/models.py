#===============================================================================
# #!/usr/bin/python
# # -*- coding: UTF-8 -*-
# 
# from django.db import models
# 
# from django.utils import timezone
# from django.utils.html import escape
# 
# 
# # Create your models here.
# class SupportThread(models.Model):
#     user=models.ForeignKey('auth.User', null=True, on_delete=models.CASCADE)
#     #order=models.ForeignKey('order.Order', null=True, on_delete=models.CASCADE)
#     
#     order_2d = models.ForeignKey('order.Order_2d', null=True, on_delete=models.CASCADE)
#     order_sp = models.ForeignKey('order.SpecialOrder', null=True, on_delete=models.CASCADE)
#     order_ex = models.ForeignKey('order.Exterior', null=True, on_delete=models.CASCADE)
#     order_dec = models.ForeignKey('order.Decoration', null=True, on_delete=models.CASCADE)
#     order_rf = models.ForeignKey('order.Roof', null=True, on_delete=models.CASCADE)
#     
#     order_type= models.PositiveSmallIntegerField(null=True)
#     subject=models.CharField(max_length=100)
#     is_closed = models.BooleanField(default=False)
#     thread_number = models.CharField(max_length=200, null=True, default = 0)
# 
#     types = (
#           (1,'service_sup'),
#           (2,'etc'),
#           #(3,'پکیج پستی'),
#           )
#     supportType = models.PositiveSmallIntegerField(choices=types, default=0)
#     
# 
# class Support(models.Model):
#     
#     thread = models.ForeignKey('SupportThread', null=True, on_delete=models.CASCADE)
#     
#     #user=models.ForeignKey('auth.User')
#     date=models.DateTimeField(auto_now_add=True)
#     #subject=models.CharField(max_length=100)
#     context=models.TextField(null = True)
#     reply=models.TextField(null = True)
#     reply_date=models.DateTimeField(null=True)     #auto_now=True hazf shod  => b jash bayad dar view set kardane tarikh anjam shavad
#     replyer = models.ForeignKey('auth.User', null=True, on_delete=models.CASCADE)
#     
#     #is_replied=models.BooleanField(default=False)
#     #is_solved=models.BooleanField(default=False)
#     #result=models.TextField(null = True)
#     #result_date=models.DateTimeField(null=True)  #auto_now=True hazf shod
#     #supportType=models.CharField(max_length=100, null = True)
#     #support_code = models. CharField (max_length = 100, null = True)
#     def saveSupport(self):
#         self.save()
#         
# 
#===============================================================================
