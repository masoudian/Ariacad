from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend

# it is used for logging-in the user after password reset 
class PasswordlessAuthBackend(ModelBackend):
    """Log in to Django without providing a password.

    """
    def authenticate(self, username, passwordless=False):
        if not passwordless:
            return None
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None