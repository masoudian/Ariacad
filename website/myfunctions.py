from product.models import Plan
import datetime
from django.utils.timezone import utc
from website.jdate import gregorian_to_jd, jd_to_persian, persian_to_jd, jd_to_gregorian

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.core.validators import validate_email
from django.core.exceptions import ValidationError

from operator import or_
from userInformation.models import Profile
#from order.models import Order
import functools
from hashids import Hashids

def search(form):
    planUnits=form.cleaned_data.get('planUnits')
    planLength=form.cleaned_data['planLength']
    planWidth=form.cleaned_data['planWidth']
    planStories=form.cleaned_data.get('planStories')
    planGarage=form.cleaned_data.get('planGarage') #not in use!
    planKrooki=form.cleaned_data.get('planKrooki')
    planRooms=form.cleaned_data.get('planRooms')
    
    #print 'planKrookiiiiiiiiiii', planKrooki
    #planKrooki = [u'1']
    
    plans=Plan.objects.filter(  plan_krooki=planKrooki[0], plan_show=True  ) 
    if planLength is not None:
        plans=plans.filter(plan_length__lte=1.15*planLength, plan_length__gte=0.85*planLength )
    if planWidth is not None:
        plans=plans.filter(plan_width__lte=1.1*planWidth, plan_width__gte=0.9*planWidth )
    
    #===========================================================================
    # plans=Plan.objects.filter(plan_width__lte=1.1*planWidth, plan_width__gte=0.9*planWidth,
    #                             plan_length__lte=1.15*planLength, plan_length__gte=0.85*planLength,
    #                             plan_krooki=planKrooki[0],
    #                             plan_show=True
    #                             )   # filter ezafe shavad plan_show=true 
    #===========================================================================
        
    if planUnits:
        
        if '4' in planUnits:
            s1={'plan_units__gte': 4 }
            plans1 = plans.filter(**s1)
            
            planUnits.remove('4')
            s2={'plan_units__in':planUnits}
            plans2 =  plans.filter(**s2)
            #print 'plans2', plans2
            
            querysets = [plans1, plans2,]  
            plans = functools.reduce(or_, querysets[1:], querysets[0])  
        else:
            s={'plan_units__in':planUnits}
            plans=plans.filter(**s)
        
    #============================  
    if planGarage:      #currently unused - it doesn't exist in the search form
        s={'plan_garage__in':planGarage}
        plans=plans.filter(**s)
    #============================
    if planRooms:
        if '4' in planRooms:
            s1={'plan_rooms__gte': 4 }
            plans1 =  plans.filter(**s1)
            
            #remove 4 from the list and filter the plans
            planRooms.remove('4')
            s2={'plan_rooms__in':planRooms}
            plans2 =  plans.filter(**s2)
        
            #merge the two previous filters to one
            querysets = [plans1, plans2,]  # List of querysets you want to combine.
            plans = functools.reduce(or_, querysets[1:], querysets[0])  # Objects that are present in *at least one* of the queries
        else:
            #print 'no 4'
            s={'plan_rooms__in':planRooms}
            plans=plans.filter(**s)
            
    if planStories:
        #if the user selected +4 : filter >=4
        if '4' in planStories:
            s1={'plan_stories__gte': 4 }
            plans1 =  plans.filter(**s1)
            
            #remove 4 from the list and filter the plans
            planStories.remove('4')
            s2={'plan_stories__in':planStories}
            plans2 =  plans.filter(**s2)
        
            #merge the two previous filters to one
            querysets = [plans1, plans2,]  # List of querysets you want to combine.
            plans = functools.reduce(or_, querysets[1:], querysets[0])  # Objects that are present in *at least one* of the queries
        else:
            #print 'no 4'
            s={'plan_stories__in':planStories}
            plans=plans.filter(**s)
    
    
    type(planKrooki)
    s={'plan_krooki':planKrooki}
    #plans=plans.filter(**s)
    #print 'planKrooki', planKrooki
            
            
    return plans
##################################################################################################################

def adsearch(plans, form): # , simple search filters
    
    planCode=form.cleaned_data['planCode']
    planLength=form.cleaned_data['planLength']
    planWidth=form.cleaned_data['planWidth']
    planSubstructure=form.cleaned_data['planSubstructure']
    planStories=form.cleaned_data.get('planStories')
    planUnits=form.cleaned_data.get('planUnits')
    planRooms=form.cleaned_data.get('planRooms')
    planGarage=form.cleaned_data.get('planGarage')
    planKrooki=form.cleaned_data.get('planKrooki')
    
    planApartment=form.cleaned_data['planApartment']
    planSeparateEntry=form.cleaned_data['planSeparateEntry']
    planPilot=form.cleaned_data.get('planPilot')
    planElevator=form.cleaned_data.get('planElevator')
    planDublex=form.cleaned_data.get('planDublex')
    
    planIssued=form.cleaned_data['planIssued']
    planSpecial=form.cleaned_data.get('planSpecial')
    
    plans=Plan.objects.filter(plan_width__lte=1.1*planWidth, plan_width__gte=0.9*planWidth,
                                plan_length__lte=1.15*planLength, plan_length__gte=0.85*planLength,
                                plan_krooki=planKrooki[0], plan_show=True
                                )
    
    
    
    plans=plans.filter(plan_dublex=planDublex,
                        plan_apartment=planApartment,
                        plan_pilot=planPilot,
                        plan_separateEntry=planSeparateEntry,
                        plan_elevator=planElevator
                        )
    
    
    if planDublex:
        s={'plan_dublex__in':planDublex}
        plans=plans.filter(**s)
    if planApartment:
        s={'plan_apartment__in':planApartment}
        plans=plans.filter(**s)
    if planIssued:
        s={'plan_issued__in':planIssued}
        plans=plans.filter(**s)
    if planSpecial:
        s={'plan_special__in':planSpecial}
        plans=plans.filter(**s)
    if planPilot:
        s={'plan_pilot__in':planPilot}
        plans=plans.filter(**s)
                #return HttpResponse("Search Results")
    #print 'plans2 %d' % plans.count()
    return plans
##################################################################################################################

def calculate_time_elapsed(time, inhours=0):
    now = datetime.datetime.utcnow().replace(tzinfo=utc)
    elapsedtime  = now - time
    elapsedtime_days = elapsedtime.days
    elapsedtime_hours = elapsedtime.seconds // 3600
    elapsedtime_minutes = (elapsedtime.seconds//60)%60
    
    if inhours == 1:
        return elapsedtime.total_seconds()/3600
    return (elapsedtime_days, elapsedtime_hours, elapsedtime_minutes)
##################################################################################################################

def has_gaurantee (time):
    days, hours, minutes = calculate_time_elapsed(time)
    if days <  2:
        guarantee_valid = True
    elif days == 2:
        if hours < 1:
            guarantee_valid = True
        else:
            guarantee_valid = False
    else:
        guarantee_valid = False
        
    return guarantee_valid
##################################################################################################################

def paginating(plan_list, page):
    
    paginator = Paginator(plan_list, 1)
            
    try:
        plans = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        plans = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        plans = paginator.page(paginator.num_pages)
    
    return plans
##################################################################################################################
     
#added by SOL

def ValidateEmail( email ):    
    try:
        validate_email( email )
        return True
    except ValidationError:
        return False
##################################################################################################################

def generateOrderCode(order_id):
    alphabet='ABCEFHKLORSTWXYZ'
    # salt is used to defend against dictionary attacks or against its hashed equivalent - optional
    hashids = Hashids(min_length=6, salt='aria_cad159', alphabet=alphabet)
    
    return hashids.encode(order_id)
##################################################################################################################

def dateToShamsi(date, timeFlag=False):
    if date == None:
        return 
    jalaliDate = gregorian_to_jd(date.year, date.month, date.day)    ## miladi be shamsi
    jalaliDate = jd_to_persian(jalaliDate)
    #add the time (in local timezone)
    if timeFlag == True:
        jdate = str(jalaliDate[0]) + '/' + str(jalaliDate[1]) + '/' + str(jalaliDate[2])
        
    else:   #add the time (in local timezone)#add the time (in local timezone)
        jdate = str(jalaliDate[0]) + '/' + str(jalaliDate[1]) + '/' + str(jalaliDate[2]) #+ ' ' + str(pdate.hour)

    return jdate
##################################################################################################################

def dateToMiladi(date):
    jd = persian_to_jd(date.year, date.month, date.day)
    bd_miladi = jd_to_gregorian(jd)                
    mdate = datetime.date(int(bd_miladi[0]), int(bd_miladi[1]), int(bd_miladi[2]))

    return mdate
##################################################################################################################

def checkUserProfile(user):
    
    profile = Profile.objects.get(user=user)
    
    if (profile.telephone == "" or profile.telephone == None or
        profile.cellphone == "" or profile.cellphone == None or
        profile.address == "" or profile.address == None or
        profile.postal_code == "" or profile.postal_code == None or
        profile.gender == "" or profile.gender == None or
        profile.nationalID == "" or profile.nationalID == None or
        profile.BirthDate == "" or profile.BirthDate == None or
        profile.cellphone == "" or profile.cellphone == None):
        
        return False
    else:
        return True
        
        



















