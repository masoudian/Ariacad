from django.contrib import admin
from articles.models import Article, Articlefile
from order.models import Order_2d, SpecialOrder, Exterior, Payment, Modification_2d
from product.models import Plan, PlanFile, Story, UnitType, PlanClass, ExtraPrice # , PlanCover
#from assistance.models import Support
from userInformation.models import Profile

# Register your models here.
class StoryInline(admin.StackedInline):
    model = Story
    extra = 1
    
    
#===============================================================================
# class UnitInline(admin.StackedInline):
#     model = Unit
#     extra = 1
#===============================================================================
    
    
class UnitTypeInline(admin.StackedInline):
    model = UnitType
    extra = 1    
    
    
class planfilesInline(admin.StackedInline):
    model = PlanFile
    extra = 1

#===============================================================================
# class planCoverInline(admin.StackedInline):
#     model = PlanCover    
#===============================================================================

class PlanAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Plan Code', {'fields': ['plan_code']}),
        ('Plan Cover', {'fields': ['cover_img']}),
        ('plan class', {'fields': ['plan_class']}),
        ('Plan Description', {'fields': ['plan_description']}),
        ('Plan Length', {'fields': ['plan_length']}),
        ('Plan Width',               {'fields': ['plan_width']}),
        ('Plan Substructure',               {'fields': ['plan_substructure']}),
        ('lighting',               {'fields': ['plan_krooki']}),
        ('Plan Stories',               {'fields': ['plan_stories']}),
        ('Plan Units',               {'fields': ['plan_units']}),
        ('Rooms',               {'fields': ['plan_rooms']}),
        ('Plan Garage',               {'fields': ['plan_garage']}),
        ('Extra facilities',               {'fields': ['plan_extraFacility']}),
        #('Closet',               {'fields': ['plan_closet']}),
        ('Pilot',               {'fields': ['plan_pilot']}),
        ('Apartment',               {'fields': ['plan_apartment']}),
        ('Is Issued',               {'fields': ['plan_issued']}),
        ('Dublex',               {'fields': ['plan_dublex']}),
        ('Separate Entry',               {'fields': ['plan_separateEntry']}),
        ('Has Elevator',               {'fields': ['plan_elevator']}),
        ('Is Special',               {'fields': ['plan_special']}),
        ('Show Plans',               {'fields': ['plan_show']}),
        
    ]
    inlines = [UnitTypeInline, StoryInline, planfilesInline] #UnitInline, , planCoverInline
    list_display = ('id','plan_code', 'plan_length', 'plan_width')
    list_filter = ['plan_code', 'plan_apartment']
    search_fields = ['plan_code', ('apartment', {'fields':['plan_apartment']})]
    
    
class SupportAdmin (admin.ModelAdmin):
    list_display = ('context',)
    
    
class articlefilesInline(admin.StackedInline):
    model = Articlefile
    extra = 1
    
    
class ArticleAdmin(admin.ModelAdmin):
    inlines = [articlefilesInline]
    fieldsets = [
     ('Content',               {'fields': ['article_content']}),
     ('Title',               {'fields': ['article_title']}),
    ]
    list_display = ('article_title', 'article_content')
    
    
class SpecialOrderAdmin(admin.ModelAdmin):
    fieldsets = [
     #('Description',               {'fields': ['description']}),
     ('Story',               {'fields': ['story']}),
    ]
    list_display = ('story',) #'description',
    
class Order_2dAdmin(admin.ModelAdmin):
    list_display = ('order_code', 'plan', 'planLength', 'planWidth' )  #'user', 'description', 

class ProfileAdmin(admin.ModelAdmin):
    fieldsets = [
        ('email_confirmed', {'fields':['email_confirmed']}),
        ]

class ExtraPriceAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'cad_price_percent', 'post_price')

class Activate2d(Order_2d):
    class Meta:
        proxy = True
        
class Activate2dAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Active', {'fields': ['active']}),
        ('status', {'fields': ['order_status']}),
        ('prev_status_date', {'fields': ['prev_status_date']}),
        ('last_status_date', {'fields': ['last_status_date']}),
        ]

class ExteriorAdmin(admin.ModelAdmin):
    fieldsets = [
        ('user', {'fields':['user']}),
        ('or2d', {'fields':['order_2d']}),
        #('or sp', {'fields':['order_sp']}),
        ('order_code', {'fields':['order_code']}),
        
        ]


admin.site.register(Plan, PlanAdmin)
admin.site.register(Order_2d, Order_2dAdmin) #admin.site.register(Modification, ModificationAdmin) 
admin.site.register(Article, ArticleAdmin)
admin.site.register(UnitType)
admin.site.register(Story)
admin.site.register(SpecialOrder, SpecialOrderAdmin)
#admin.site.register(Support, SupportAdmin)
admin.site.register(Profile, ProfileAdmin )
admin.site.register(PlanClass)
admin.site.register(ExtraPrice, ExtraPriceAdmin)
admin.site.register(Exterior, ExteriorAdmin)
admin.site.register(Modification_2d)

''' !!!this is only used in production tests!!! '''
admin.site.register(Activate2d, Activate2dAdmin) 
admin.site.register(Payment)
