#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.forms import DateField
from django.contrib.auth.models import User
from product.models import Plan, PlanComment
from userInformation.models import Profile
from django.forms.fields import CharField
#from assistance.models import Support, SupportThread
#from order.models import SpecialOrder, Order_2d, Modification

#added by sol
import re
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from website.myfunctions import ValidateEmail
from django.forms.utils import ErrorList
from nocaptcha_recaptcha.fields import NoReCaptchaField
from django.contrib.auth.forms import PasswordResetForm, SetPasswordForm,\
    PasswordChangeForm

from django.template import loader
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth import authenticate 
from django.dispatch import receiver
from django.db.models.signals import post_save
 
from django.contrib.auth.password_validation import MinimumLengthValidator
from order.models import Modification_2d




# to lower case the username input
class UsernameField(forms.EmailField):
    def to_python(self, value):
        return value.lower()
    
# sign up form
class UserForm (forms.ModelForm):

    captcha = NoReCaptchaField()
    username = UsernameField()
    
    class Meta:
        model = User
        fields = ('username', 'password')

    #field validation 
    def clean(self):
        email = self.data['username']
        password = self.cleaned_data.get('password', '')

        # validate email 
        if email == '':
            self._errors["username"] = ErrorList([u"پست الکترونیک را وارد نمایید."])
        
        elif ValidateEmail(email) is False :
            self._errors["username"] = ErrorList([u"فرمت وارد شده صحیح نمی باشد."])
        
        elif User.objects.filter(email=email).exists():
            self._errors["username"] = ErrorList([u"آدرس پست الکترونیک شما در سیستم موجود است. اگر قبلاً با این آدرس ثبت نام کرده اید برای ورود به سایت می توانید از لینک زیر استفاده کنید. در غیر اینصورت برای ثبت نام آدرس پست الکترونیک دیگری وارد نمایید."])
            
        
        # validate password
        if password == '':
            self._errors["password"] = ErrorList([u"رمز عبور را وارد نمایید."])
            
        elif len(password)<4:
            self._errors["password"] = ErrorList([u"طول کلمه عبور باید حداقل 4 کاراکتر باشد."])
            
        
        #validate recaptcha field
        #if 
        
        return self.cleaned_data 

#===========================================================================
  
class LoginForm (forms.ModelForm):
    
    #captcha = NoReCaptchaField()
    
    class Meta:
        #model information
        #fields = ('email', 'password','passowrd_2', 'telephone', '')
        model = User
        fields = ('username', 'password')     
        
    #field validation 
    def clean(self):
        email = self.data['username']
        password = self.cleaned_data.get('password', '')

        # validate email 
        if email == '':
            self._errors["username"] = ErrorList([u"لطفا پست الکترونیک را وارد نمایید."])
        
        elif ValidateEmail(email) is False :
            self._errors["username"] = ErrorList([u"فرمت وارد شده صحیح نمی باشد."])
        
        # validate password
        if password == '':
            self._errors["password"] = ErrorList([u"لطفا رمز عبور را وارد نمایید."])
        
        else:
            user = authenticate(username=email, password=password)
            if not user or not user.is_active:
                raise forms.ValidationError("نام کاربری یا کلمه عبور نامعتبر است.")
        return self.cleaned_data
     
    def login(self, request):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        return user
        #=======================================================================
        # model = Profile
        # fields = ('cellphone',)
        # exclude = ['user']
        #=======================================================================
'''
    def save(self, commit=True):
        extra_field = self.cleaned_data.get('extra_field', None)
        # ...do something with extra_field here...
        return super(YourModelForm, self).save(commit=commit)
''' 
#===========================================================================
class editUserForm(forms.ModelForm):
    class Meta: 
        model = User
        fields = ('first_name', 'last_name')


class ProfileForm(forms.ModelForm):
    class Meta: 
        model = Profile
        fields = ('cellphone', 'telephone', 'gender', 'nationalID')
        exclude = ['user']


#===============================================================================
# @receiver(post_save, sender=User)
# def update_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)
#     instance.profile.save()
#===============================================================================

        
        
class editProfileForm(forms.ModelForm):
    BirthDate =  forms.DateField (input_formats=['%Y/%m/%d'])   #default format is m,d,y
    class Meta:
        model = Profile
        fields = ('cellphone', 'telephone', 'gender', 'nationalID', 'address', 'postal_code', 'magazine')
        
#===========================================================================
        
class SimpleSearchForm (forms.Form):
    
    # class Meta:
        # model = Plan
        # fields =('planLength', 'planWidth', 'planUnits', 'planRooms', 'planGarage','planStories')
    story_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         )
    room_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         )
    unit_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),

         )
    krooki_options = (
        (1, 'north'),
        (2, 'south'),
        (3, 'north_south'),
        (4, 'do_nabsh'),
        (5, 'vila'),
                    )
    planLength = forms.FloatField(label='length', required=False)
    planWidth = forms.FloatField(label='width', required=False)
    planUnits = forms.MultipleChoiceField(choices=unit_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of units')
    planKrooki = forms.MultipleChoiceField(choices=krooki_options, required=False, widget=forms.CheckboxSelectMultiple(), label='plan position')
    planRooms = forms.MultipleChoiceField(choices=room_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of rooms')
    planStories = forms.MultipleChoiceField(choices=story_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of stories')
    
    def clean_planLength(self):
        data = self.cleaned_data['planLength']
        '''
        if data != 2:
            print 'error gotten'
            raise forms.ValidationError("You have forgotten about Fred!")
        '''
        return data
#===========================================================================
    
class AdvancedSearchForm (forms.Form):
    
    # class Meta:
        # model = Plan
        # fields =('planLength', 'planWidth', 'planUnits', 'planRooms', 'planGarage','planStories')
    store_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         (5, 5),
         (6, 6),
         )
    room_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         (5, 5),
         (6, 6),
         )
    garage_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         (5, 5),
         (6, 6),
         )
    unit_options = (
         (1, 1),
         (2, 2),
         (3, 3),
         (4, 4),
         (5, 5),
         (6, 6),
         )
    apartment = (
        (True,'Apartment'), 
        (False,'Villa'),  
             )
    dublex = (
        (True, 'Dublex'),
        (False, 'Simple'),    
             )
    issued = (
        (True, 'Only Issued'),
        (False, 'Only Not Issued'),    
             )
    pilot = (
        (True, 'Only Pilot'),
        (False, 'Only Ground'),    
             )
    special = (
        (True, 'Only Special'),
        (False, 'Only Not Special'),    
             )
    planLength = forms.FloatField(label='length')
    planWidth = forms.FloatField(label='width')
    planUnits = forms.MultipleChoiceField(choices=unit_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of units')
    planGarage = forms.MultipleChoiceField(choices=garage_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of garages')
    planRooms = forms.MultipleChoiceField(choices=room_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of rooms')
    planStories = forms.MultipleChoiceField(choices=store_options, required=False, widget=forms.CheckboxSelectMultiple(), label='number of stories')
    planApartment = forms.MultipleChoiceField(choices=apartment, required=False, widget=forms.CheckboxSelectMultiple())
    planDublex = forms.MultipleChoiceField(choices=dublex, required=False, widget=forms.CheckboxSelectMultiple())
    planIssued = forms.MultipleChoiceField(choices=issued, required=False, widget=forms.CheckboxSelectMultiple())
    planPilot = forms.MultipleChoiceField(choices=pilot, required=False, widget=forms.CheckboxSelectMultiple())
    planSpecial = forms.MultipleChoiceField(choices=special, required=False, widget=forms.CheckboxSelectMultiple())
#===========================================================================

class OrderForm(forms.Form):
    order_type = (
         (1, '2d'),
         (2, '2d+3d'),
         (3, '3d'),
        )

    order_choice = forms.MultipleChoiceField(choices=order_type, widget=forms.CheckboxSelectMultiple(), label='type of order')
#===========================================================================

#===============================================================================
# class Order_2dForm(forms.ModelForm):     #ModificationForm name was changed to Order_2dForm
#     
#     krooki_file = forms.FileField(required=False)
#     
#     class Meta:
#         model = Order_2d
#         fields = ('state', 'planLength', 'planWidth', 'roof', 'lotLength', 'lotWidth', 'cad', 'post','krooki_file')
#         
#         
#     #field validation 
#     def clean(self):
#         if self.cleaned_data.get('state', '') == '':
#             self._errors["state"] = ErrorList([u"این فیلد الزامی است.1"])
#         
#         if self.cleaned_data.get('planLength', '') == '':
#             self._errors["planLength"] = ErrorList([u"این فیلد الزامی است.2"])
#             
#         if self.cleaned_data.get('planWidth', '') == '':
#             self._errors["planWidth"] = ErrorList([u"این فیلد الزامی است.3"])
#             
#         if self.cleaned_data.get('roof', '') == '':
#             self._errors["roof"] = ErrorList([u"این فیلد الزامی است.4"])
#             
#         if self.cleaned_data.get('lotLength', '') == '':
#             self._errors["lotLength"] = ErrorList([u"این فیلد الزامی است.5"])
#             
#         if self.cleaned_data.get('lotWidth', '') == '':
#             self._errors["lotWidth"] = ErrorList([u"این فیلد الزامی است.6"])
# 
#         return self.cleaned_data    
#===============================================================================
#===========================================================================

#===============================================================================
# class SpForm(forms.ModelForm):  #support form
#     
#     class Meta: 
#         model = Support
#         fields = ( 'context',)
# 
# class SpThreadForm(forms.ModelForm):
#     class Meta:
#         model = SupportThread
#         fields = ('supportType', 'subject',)
#===============================================================================
#===========================================================================

#===============================================================================
# class SpecialOrderForm(forms.ModelForm):
#      
#     class Meta: 
#         model = SpecialOrder
#         fields = ('dimension', 'progression', 'story', 'room', 'parking', 'area', 'roof','krooki_file', 'area_file')  #'description' was moved to (late)modification form
# #===========================================================================
#  
#===============================================================================
class ModificationForm(forms.ModelForm):
         
    class Meta:
        model = Modification_2d
        fields = ('req_description',)
         
    def clean(self):
        if self.cleaned_data.get('req_description', '') == '':
            self._errors["req_description"] = ErrorList([u"این فیلد الزامی است."])

class CommentForm(forms.ModelForm):
    
    class Meta:
        model = PlanComment
        fields = ('person_name', 'person_email', 'comment_text')
     
#===========================================================================
#to have the captcha on the reset form had to use this, 
#the contents is copied from django PasswordResetForm
class CaptchaPasswordResetForm(PasswordResetForm):
    captcha = NoReCaptchaField()        #added by sol!
    email = forms.EmailField(  error_messages = { 'invalid': _("فرمت وارد شده صحیح نمی باشد."),
                                                  'required': _("لطفا آدرس پست الکترونیک خود را وارد نمایید.")  } )

#===========================================================================
# min length validator
class MyMinimumLengthValidator(MinimumLengthValidator):
    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                "رمز عبور باید حداقل {0} کاراکتر باشد  (حرف، عدد یا ترکیبی از آنها).".format(self.min_length),
                code='password_too_short',
                params={'min_length': self.min_length},
            )

class MySetPasswordForm(SetPasswordForm):
    
    error_messages = {
        'password_mismatch': _("تکرار کلمه عبور صحیح نیست."),
    }
    
    def __init__(self, *args, **kwargs):
        super(MySetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password1'].error_messages['required'] = _("لطفا کلمه  عبور جدید را وارد نمایید.")
        self.fields['new_password2'].error_messages['required'] = _("لطفا تکرار کلمه عبور جدید را وارد نمایید.")

#===========================================================================        
        
class MyPasswordChangeForm(PasswordChangeForm):
    
    error_messages = {
        'password_incorrect': _("کلمه عبور قدیمی شما صحیح نمی باشد. لطفا دوباره وارد کنید."),
        'password_mismatch': _("تکرار کلمه عبور صحیح نیست."),
    }

    def __init__(self, *args, **kwargs):
        super(MyPasswordChangeForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].error_messages['required'] = _("لطفا کلمه  عبور قدیمی را وارد نمایید.")
        self.fields['new_password1'].error_messages['required'] = _("لطفا کلمه  عبور جدید را وارد نمایید.")
        self.fields['new_password2'].error_messages['required'] = _("لطفا تکرار کلمه عبور جدید را وارد نمایید.")





        
        