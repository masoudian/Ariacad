#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Django settings for mainwork project.
"""
import dj_database_url
from decouple import Csv, config
import os

## Email and SMS backends ######################################################
EMAIL_USE_TLS = config('EMAIL_USE_TLS', cast=bool)
EMAIL_USE_SSL = config('EMAIL_USE_SSL', cast=bool)
EMAIL_HOST = config('EMAIL_HOST')
EMAIL_PORT = config('EMAIL_PORT', cast=int)
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = EMAIL_HOST_USER
PASSWORD_RESET_TIMEOUT_DAYS = 2  #default is 3 days

#for logging-in the user after password reset
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'website.backends.PasswordlessAuthBackend'
)

SENDSMS_BACKEND =  'sendsms.backends.console.SmsBackend'
AUTH_PROFILE_MODULE = 'userInformation.Profile'


#===============================================================================
# ADMINS = (
#     ('Joe Lennon', 'admin_email'),
# )
#===============================================================================


## DIRs and ROOTs ##############################################################

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# The directory where collectstatic command copies/symlinks the files to
STATIC_URL = '/static/'
PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

STATIC_ROOT = os.path.join(PROJECT_DIR, 'static')

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'


# The directory where different applications uploads media files to
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'      #set this to ariacad.com/media  for the server


LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

ROOT_URLCONF = 'mainwork.urls'

## !!SECURE THESE AT PRODUCTION!! ##############################################
SECRET_KEY = config('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', default=False, cast=bool)

#Database
DATABASES = {
    'default': dj_database_url.config(
        default=config('DATABASE_URL')
    )
}

# NoRecaptcha settings -- !!delete it for deployment!!
#RECAPTCHA_PUBLIC_KEY = config('RECAPTCHA_PUBLIC_KEY')
#RECAPTCHA_PRIVATE_KEY = config('RECAPTCHA_PRIVATE_KEY')
NORECAPTCHA_SITE_KEY = config('NORECAPTCHA_SITE_KEY')
NORECAPTCHA_SECRET_KEY = config('NORECAPTCHA_SECRET_KEY')
NOCAPTCHA = config('NOCAPTCHA', cast=bool)
#NORECAPTCHA_PROXY = config('NORECAPTCHA_PROXY')

# Recaptcha settings
# this is just for testing -- !! delete it for deployment !!
GR_CAPTCHA_SECRET_KEY = config('GR_CAPTCHA_SECRET_KEY')
# this is the real sercret key -- !! uncomment it for deployment !!
#GR_CAPTCHA_SECRET_KEY = config('GR_CAPTCHA_SECRET_KEY')

## Miscellaneous ###############################################################

ALLOWED_HOSTS = ['localhost', '127.0.0.1', 'ariacad.herokuapp.com']

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'cloudinary_storage',   # cloud storage (cloudinary)
    'cloudinary',           # cloud storage

    'django_filters',
    'product',
    'website',
    'order',
    'assistance',
    'userInformation',
    'portfolio',
#    'management',
    'articles',
    'rest_framework',
    'widget_tweaks',     #this is to add css to form fields {{ }}
    'nocaptcha_recaptcha',   # this does not work for django REST (for now?)
    'recaptcha',      # added this for REST
    'debug_toolbar',
    
    #'storages',         # for adding dropbox
    'corsheaders',      # to serve front end requests 
)


MIDDLEWARE_CLASSES = (
    
    'debug_toolbar.middleware.DebugToolbarMiddleware',      #added by sol - for the purpose of django-debug-tools
                      
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    
    'corsheaders.middleware.CorsMiddleware',        # to serve front end requests-should be before CommonMiddleware
    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',     #added by sol - for message translations
    

)
INTERNAL_IPS = ('127.0.0.1',)   #added by sol - for the purpose of django-debug-tools


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],     #os.path.join(os.path.dirname(__file__), 'templates'),
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'mainwork.wsgi.application'


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

#LANGUAGE_CODE = 'fa'

TIME_ZONE = 'Asia/Tehran'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# The languages you are supporting
LANGUAGES = (
    #('fa', 'Farsi'),   # You need to include your LANGUAGE_CODE language
)

# for password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        #'NAME': 'website.forms.MyMinimumLengthValidator', #'django.contrib.auth.password_validation.MinimumLengthValidator',
        'NAME': 'userInformation.api.validators.MyPasswordValidator', #'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        }
    },    
]


# rest framework error message override
from rest_framework import fields
from rest_framework import serializers  
from django.utils.translation import ugettext_lazy
fields.Field.default_error_messages = {
    'required': ugettext_lazy("این فیلد الزامی است!"),
}
serializers.CharField.default_error_messages['blank'] = "این فیلد الزامی است!"
serializers.EmailField.default_error_messages['invalid'] = "لطفاً یک پست الکترونیک معتبر وارد نمایید."  
#  "پیت الکترونیک وارد شده معتبر نیست!"



REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
}

import datetime
JWT_AUTH = {
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_ALLOW_REFRESH': True,
}


#cloudinary settings 
from django.conf import settings
CLOUDINARY_STORAGE = {
    'CLOUD_NAME': 'dlbxbieg8',
    'API_KEY': '755572643459959',
    'API_SECRET': 'wjlAZviKXWqHMaKKkKTzKpLrlrY',
    'SECURE': True,
    'MEDIA_TAG': 'media',
    'PREFIX': settings.MEDIA_URL
}

if not DEBUG:
    DEFAULT_FILE_STORAGE = 'cloudinary_storage.storage.MediaCloudinaryStorage'

#===============================================================================
# # for dropbox
# DEFAULT_FILE_STORAGE = 'storages.backends.dropbox.DropBoxStorage'
# #DROPBOX_OAUTH2_TOKEN = '48_WqcTW8HAAAAAAAAAADdkdKmYrDqCFNkLDR26ZDYgaEDz5E7U0v6BX1phaLae-'
# DROPBOX_OAUTH2_TOKEN = '48_WqcTW8HAAAAAAAAAADi8wXnv0IhpWKLYkve6G7nSJyiBYe55B2nvsNLknSdMC'
# DROPBOX_ROOT_PATH = 'media'
#===============================================================================

CORS_ORIGIN_ALLOW_ALL=True      # to serve front end requests

CORS_ALLOW_METHODS = (
        'GET',
        'POST',
        'PUT',
        'PATCH',
        'DELETE',
        'OPTIONS'
)

CORS_ALLOW_HEADERS = (
        'x-requested-with',
        'content-type',
        'accept',
        'origin',
        'authorization',
        'x-csrftoken'
)


#===============================================================================
# # enable logging
# if DEBUG:
#     LOGGING = {
#         'version': 1,
#         'disable_existing_loggers': False,
#         'formatters': {
#             'verbose': {
#                 'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
#                 'datefmt' : "%d/%b/%Y %H:%M:%S"
#             },
#             'simple': {
#                 'format': '%(levelname)s %(message)s'
#             },
#         },
#         'handlers': {
#             'file': {
#                 'level': 'DEBUG',
#                 'class': 'logging.FileHandler',
#                 'filename': 'mysite.log',
#                 'formatter': 'verbose'
#             },
#         },
#         'loggers': {
#             'django': {
#                 'handlers':['file'],
#                 'propagate': True,
#                 'level':'DEBUG',
#             },
#             'MYAPP': {
#                 'handlers': ['file'],
#                 'level': 'DEBUG',
#             },
#         }
#     } ###
#===============================================================================

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
    },
}
    
