"""mainwork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import django.views.static
from rest_framework.documentation import include_docs_urls
from rest_framework.permissions import AllowAny


#from django.urls import path

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
#    url(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT}),


   url(r'^docs/', include_docs_urls(title='Ariacad API', permission_classes=[AllowAny])),
   
   url(r'^api/auth/token/', obtain_jwt_token),
   url(r'^api/token/refresh/', refresh_jwt_token),
   
   url(r'^api/plans/', include("product.api.urls", namespace='plans-api')),
   url(r'^api/users/', include("userInformation.api.urls", namespace='users-api')),
   url(r'^api/orders/', include("order.api.urls", namespace='orders-api')),
   #url(r'^', include('django.contrib.auth.urls')),        #ehtemalan dar django version-haye balatar niaz bashd
   
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    #urlpatterns += url(r'', include('website.urls')),
    #urlpatterns += url(r'', include('management.urls')),


#for django-debug-toolbar
if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
    
    
    