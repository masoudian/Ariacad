#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.core.exceptions import ValidationError

class MyPasswordValidator(object):
    
    def __init__(self, min_length=8):
        self.min_length = min_length
        
    def validate(self, password, user=None):
        if len(password) < self.min_length:
            raise ValidationError(
                "کلمه عبور باید حداقل {0} کاراکتر باشد  (ترکیبی از اعداد و حروف لاتین).".format(self.min_length),
                code='password_too_short',
                params={'min_length': self.min_length},
            )

        elif password.isdigit():
            raise ValidationError(
                "رمز عبور باید حداقل {0} کاراکتر باشد  (حرف، عدد یا ترکیبی از آنها).".format(self.min_length),
                code='password_entirely_numeric',
            )

