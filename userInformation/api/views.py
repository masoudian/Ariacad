#!/usr/bin/python
# -*- coding: UTF-8 -*-
from django.contrib.auth import get_user_model
from django.contrib.auth import  update_session_auth_hash
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.views.decorators.csrf import csrf_protect

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from rest_framework.generics import (
    CreateAPIView,
    RetrieveAPIView, 
    RetrieveUpdateAPIView,
    UpdateAPIView
    )



from website.tokens import account_activation_token

from rest_framework.permissions import (
    AllowAny,
    )

#from .permissions import IsProfileOwner

from userInformation.models import Profile 
from django.contrib.admin.utils import lookup_field
from rest_framework.decorators import permission_classes

User = get_user_model()

from .serializers import ( 
    UserCreateSerializer,
    UserEmailSerializer,
    UserLoginSerializer,
    AccountActivateSerializer,
    UserSerializer,
    PasswordChangeSerializer,
    PasswordResetSerializer,
    PasswordResetConfirmSerializer,
    ProfileSerializer,
    ChangeUserEmailSerializer,
    
    )

## functions

def sendConfirmationEmail(request, email):
    # send confirmation link to user's email,
    # authentication will be done after the conf link is clicked.
    user = User.objects.get(username=email)
    current_site = get_current_site(request)
    subject = 'آریاکد: فعال‌سازی حساب کاربری'
    message = render_to_string('website/account_activation_email.html', {
        'user': user,
        #'domain': current_site.domain,
        'domain': 'aria-cad.herokuapp.com',
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
    })
    user.email_user(subject, message)
    return 


def validate_link(uidb64, token, token_type):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        
    if user is not None and token_type.check_token(user, token):
        return user     # link is valid
    else:
        return None
############################################################################

class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializer
    permission_classes = [AllowAny]
    #queryset = User.objects.all() 
    
    def post(self, request, *args, **kwargs):
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            validated_data = serializer.save()
       
            sendConfirmationEmail(self.request, validated_data['email'])
            
            return Response(serializer.data, status=HTTP_200_OK)
            
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    
    
class AccountActivateAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = AccountActivateSerializer
    
    def dispatch(self, request, uidb64, token):     #is it necessary??
        self.user = validate_link(uidb64, token, account_activation_token)
        return APIView.dispatch(self, request, uidb64, token)
    
    
    def get(self, request, uidb64, token):
        
        user = validate_link(uidb64, token, account_activation_token)
        if user is None:
            return Response({'status': 'لینک فعال‌سازی نامعتبر است.'}, status=HTTP_400_BAD_REQUEST)
        
        context = {'user': self.user}    
        serializer = AccountActivateSerializer(data=request.data, context=context)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    
class ResendActivationEmail(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserEmailSerializer
    
    def post(self, request, *args, **kwargs):   #it takes username and sends the email to it
        data = request.data
        serializer = UserEmailSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            username = new_data['email']
        
            try:
                user = User.objects.get(username=username)
            except (TypeError, ValueError, OverflowError, User.DoesNotExist):
                user = None
            if user is not None :
                if user.profile.email_confirmed:
                    return Response({'msg':'حساب کاربری شما فعال است.'}, status=HTTP_400_BAD_REQUEST)
    
                sendConfirmationEmail(self.request, username)
                
                return Response({'username':username, 'msg':'لینک فعالسازی مجددا ارسال شد.'}, status=HTTP_200_OK)
            
            return Response({'username':username, 'msg':'کاربری با این پست الکترونیک در سیستم ثبت نشده است!'}, status=HTTP_400_BAD_REQUEST)
        
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    

class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer
    
    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PasswordChangeAPIView(UpdateAPIView):
    serializer_class = PasswordChangeSerializer
    
    def update(self, request):
        serializer = PasswordChangeSerializer(data=request.data)
        user = request.user
        
        if serializer.is_valid(raise_exception=True):
            # validate old password: check if it is valid
            if not user.check_password(serializer.data.get('old_password')):
                return Response({'old_password': ['رمز عبور اشتباه است.']}, status=HTTP_400_BAD_REQUEST)
            
            # set_password also hashes the password that the user will get
            user.set_password(serializer.data.get('new_password'))
            user.save()
            update_session_auth_hash(request, user)
            return Response({'status': 'رمز عبور تغییر داده شد'}, status=HTTP_200_OK)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


''' forget password '''
class PasswordResetAPIView(APIView):
    permission_classes = [AllowAny] #IsNotAuthenticated
    serializer_class = PasswordResetSerializer
     
    def post(self, request):
        serializer = PasswordResetSerializer(data=request.data)
         
        if serializer.is_valid(raise_exception=True):
            opts = {
                'use_https': request.is_secure(),
                'email_template_name': 'email/reset_email.html',
                'subject_template_name': 'email/reset_subject.txt',
                'request': request,
            }
            serializer.save(**opts )
            return Response({'status': 'ایمیل فرستاده شد.'}, status=HTTP_200_OK)
             
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
            

class PasswordResetConfirmAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = PasswordResetConfirmSerializer
    
    def dispatch(self, request, uidb64, token):
        ''' call link_validation and add its output to self variables '''      
        self.user = validate_link(uidb64, token, default_token_generator)
        return super(PasswordResetConfirmAPIView, self).dispatch(request, uidb64, token)
    
    def get(self, request, uidb64, token):
        if self.user == None:
            return Response({'status': 'لینک تغییر کلمه‌ی عبور منقضی شده است.'}, status=HTTP_400_BAD_REQUEST)
        else: 
            return Response(status=HTTP_200_OK)
    
    def post(self, request, uidb64, token):
        if self.user == None:
            return Response({'status': 'لینک تغییر کلمه‌ی عبور منقضی شده است.'}, status=HTTP_400_BAD_REQUEST)
        context = {'user': self.user}    
        serializer = PasswordResetConfirmSerializer(data=request.data, context=context)
        
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    
    
class ProfileRetrieveAPIView(RetrieveAPIView):
    serializer_class = ProfileSerializer
    
    def retrieve(self, request, *args, **kwargs):
        # Try to retrieve the requested profile and throw an exception if the
        # profile could not be found.
        try:
            # We use the `select_related` method to avoid making unnecessary
            # database calls.
            profile = Profile.objects.select_related('user').get(
                user=request.user
            )
        except Profile.DoesNotExist:
            # raise something!! like 400 and a detail description (profile does not exist)
            raise  
 
        serializer = self.serializer_class(profile)
 
        return Response(serializer.data, status=HTTP_200_OK)


class UserProfileRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    
    def retrieve(self, request, *args, **kwargs):
        # There is nothing to validate or save here. Instead, we just want the
        # serializer to handle turning our `User` object into something that
        # can be JSONified and sent to the client.
        serializer = self.serializer_class(request.user)

        return Response(serializer.data, status=HTTP_200_OK)
    
    def update(self, request, *args, **kwargs):
        serializer_data = request.data

        serializer = self.serializer_class(
            request.user, data=serializer_data,
        )
        
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    

class ChangeEmailRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = ChangeUserEmailSerializer
    
    def retrieve(self, request, *args, **kwargs):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data, status=HTTP_200_OK)
    
    def update(self, request):
        context = {'user': request.user}        
        serializer = self.serializer_class(data=request.data, context=context)
        
        if serializer.is_valid(raise_exception=True):
            
            user = request.user
            user.email = serializer.data.get('email')
            
            user.save()
            
            #serializer.save()
            return Response(serializer.data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)
    
#===============================================================================
# class LogoutAPIView(APIView):
#     
#     def logout(self, request):
#         try:
#             request.user.auth_token.delete()
#         except (AttributeError, ObjectDoesNotExist):
#             pass
#         
#         return Response({"detail": _("Successfully logged out.")},
#                         status=HTTP_200_OK)
#         
#===============================================================================
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    