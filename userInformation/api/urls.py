from django.conf.urls import url
from django.contrib import admin

from .views import (
    AccountActivateAPIView,
    UserCreateAPIView,
    UserLoginAPIView,
    UserProfileRetrieveUpdateAPIView,
    PasswordChangeAPIView,
    PasswordResetAPIView,
    PasswordResetConfirmAPIView,
    ProfileRetrieveAPIView,
    ResendActivationEmail,
    ChangeEmailRetrieveUpdateAPIView,
    )

urlpatterns = [
    url(r'^login/$', UserLoginAPIView.as_view(), name='login'),
    url(r'^register/$', UserCreateAPIView.as_view(), name='register'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        AccountActivateAPIView.as_view(), name='activate'),
    url(r'^resendemail/$', ResendActivationEmail.as_view(), name='resend-email'),
    
    url(r'^password/change/$', PasswordChangeAPIView.as_view(), name='password-change'),
    url(r'^password/reset/$', PasswordResetAPIView.as_view(), name='password-reset'),
    url(r'^password/reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
         PasswordResetConfirmAPIView.as_view(), name='password-reset-confirm'),
    
    url(r'^profile/$', ProfileRetrieveAPIView.as_view(), name='profile'),
    url(r'^profile/edit/$', UserProfileRetrieveUpdateAPIView.as_view(), name='edit-profile'),
    
    url(r'^email/change/$', ChangeEmailRetrieveUpdateAPIView.as_view(), name='email-change'),
    
    
]
