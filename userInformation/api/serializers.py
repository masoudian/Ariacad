#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db.models import Q
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
import django.contrib.auth.password_validation as validators
from django.contrib.auth.tokens import default_token_generator
from django.contrib.sites.shortcuts import get_current_site
from django.core import exceptions
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


from website.tokens import account_activation_token


from recaptcha.fields import ReCaptchaField

from userInformation.models import Profile

from rest_framework.serializers import (
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    Serializer,
    SerializerMethodField,
    ValidationError,
    )

from rest_framework_jwt.settings import api_settings
from pickle import INST


User = get_user_model()

# this will generate a token - we can instead override the 
# user manager and add a @property def token(self) to it and 
# call it like user.token whenever the token is needed.
def generate_jwt_token(user_obj):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user_obj)
    return jwt_encode_handler(payload)



class UserCreateSerializer(ModelSerializer):
    recaptcha = ReCaptchaField(write_only=True)
    email = EmailField(label='email')
    email2 = EmailField(label='confirm email')
    #token = CharField(allow_blank=True, read_only=True)
    msg = CharField(allow_blank=True, read_only=True)
    
    class Meta:
        model = User
        fields = [
                  'email', 
                  'email2', 
                  'password', 
                  #'token', 
                  'msg',
                  'recaptcha',
                 ]
        
        extra_kwargs = {"password":
                            {"write_only": True},
                        }
  
    def validate(self, data):
        email =  data.get('email',None).lower()
        email2 = data.get('email2',None).lower()
        password = data['password']
        
        user_qs = User.objects.filter(username=email)
        if user_qs.exists():
            raise ValidationError("این پست الکترونیک قبلا در سیسیتم ثبت شده است. اگر قبلاً با این آدرس ثبت نام کرده اید برای ورود به سایت می توانید از لینک زیر استفاده کنید. در غیر اینصورت برای ثبت نام آدرس پست الکترونیک دیگری وارد نمایید.")

        
        if email2 != email:
            raise ValidationError({'email2' : ["پست الکترونیک مطابقت ندارد"]})
        
        errors = []
        try:
            # validate the password and catch the exception
            validators.validate_password(password=password, user=self.instance)
        # the exception raised here is different than serializers.ValidationError
        except exceptions.ValidationError as e:
            errors = list(e.messages)
        if errors:
            raise ValidationError({'password':errors})
        
        data['email'] = email #normalized email
        return data
        
    def create(self, validated_data):    
        email = validated_data['email'].lower()
        password = validated_data['password']
        
        user = User(    #create an instance of User model
                    username = email,
                    email = email, 
                    is_active = True,
                )
        user.set_password(password)
        user.save()
        
        Profile.objects.create(user=user, last_modified = timezone.now())
        
        validated_data['msg'] = "لینک فعالسازی ارسال شد."
        #validated_data['token'] = generate_jwt_token(user)
        return validated_data


class UserEmailSerializer(ModelSerializer):
    recaptcha = ReCaptchaField(write_only=True)
    email = EmailField(label='email')
    class Meta:
        model = User
        fields = ('email', 'recaptcha')
    
    def validate_email(self, value):
        user_qs = User.objects.filter(email=value)
        if not user_qs.exists():
            raise ValidationError("این پست الکترونیک در سیستم ثبت نشده است.")
        return value
    
    
class UserLoginSerializer(ModelSerializer):
    recaptcha = ReCaptchaField(write_only=True)
    token = CharField(allow_blank=True, read_only=True)
    username = EmailField()
    class Meta:
        model = User
        fields = ('username', 'password', 'token', 'recaptcha')

        extra_kwargs = {"password":
                            {"write_only": True}
                            }

    def validate(self, data):
        username = data.get('username',None).lower()
        password = data['password']
        
        user = authenticate(username=username, password=password)
        if not user:
            raise ValidationError("پست الکترونیک  یا کلمه عبور وارد شده نامعتبر است.")
        elif not user.profile.email_confirmed:
            raise ValidationError("حهت فعالسازی حساب کاربریتان به پست الکترونیک خود مراجعه و لینک فعالسازی را کلیک نمایید.")
        #elif not user.is_active:
        #    raise ValidationError("حساب کاربری شما غیرفعال است. لطفا با ایمیل 'admin@ariacad.com' تماس بگیرید.")
        
        data['username'] = username #normalized email
        data['token'] = generate_jwt_token(user)
        
        return data


class AccountActivateSerializer(ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    username = CharField(allow_blank=True, read_only=True)
    msg = CharField(allow_blank=True, read_only=True)
    
    class Meta:
        model = User
        fields = ('username', 'token', 'msg')

    
    def save(self):
        user = authenticate(username=self.context.get('user', None).username, passwordless=True)
        if user:
            #user.is_active = True
            #user.save()
            user.profile.email_confirmed = True
            user.profile.save()
            
            self.validated_data['token'] = generate_jwt_token(user)
            self.validated_data['username'] = user.username
            self.validated_data['msg'] = "حساب کاربری فعال گردید."
        return self.validated_data



class PasswordChangeSerializer(Serializer):
    old_password = CharField(label='current password')
    new_password = CharField(label='new password')
    new_password2 = CharField(label='confirm new password')

    extra_kwargs = {
            "old_password": {"write_only": True},
            "new_password": {"write_only": True},
            "new_password2": {"write_only": True},
        }

    def validate(self, data):
        new_password = data['new_password']
        new_password2 = data['new_password2']

        # validate new password: check if it satisfies the password rules
        errors = []
        try:
            validators.validate_password(password=new_password)#, user=self.instance)
        except exceptions.ValidationError as e:
            errors = list(e.messages)
        if errors:
            raise ValidationError({'new_password':errors})
        
        # check if new_password2 matches new_password
        if new_password2 != new_password:
            raise ValidationError({'new_password2' : ["رمز عبور مطابقت ندارد"]})
        
        return data


class PasswordResetSerializer(ModelSerializer):
    recaptcha = ReCaptchaField(write_only=True)
    email = EmailField(label='email')
    class Meta:
        model = User
        fields = ('email', 'recaptcha')
    
    def validate_email(self, value):
        user_qs = User.objects.filter(email=value)
        if not user_qs.exists():
            raise ValidationError("این پست الکترونیک در سیستم ثبت نشده است.")
        return value
    
    
    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Sends a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)
            email_message.attach_alternative(html_email, 'text/html')

        email_message.send()

    def get_users(self, email):
        """Given an email, return matching user(s) who should receive a reset.

        This allows subclasses to more easily customize the default policies
        that prevent inactive users and users with unusable passwords from
        resetting their password.
        """
        active_users = get_user_model()._default_manager.filter(
            email__iexact=email, is_active=True)
        return (u for u in active_users if u.has_usable_password())

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None,
             extra_email_context=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        email = self.validated_data["email"]
        for user in self.get_users(email):
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            context = {         #this might be changed to serialized data!! ASK Mosalman!!!
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
                'date': timezone.now
            }
            if extra_email_context is not None:
                context.update(extra_email_context)
            self.send_mail(
                subject_template_name, email_template_name, context, from_email,
                user.email, html_email_template_name=html_email_template_name,
            )


class PasswordResetConfirmSerializer(ModelSerializer):
    token = CharField(allow_blank=True, read_only=True)
    new_password = CharField(label='new password', write_only=True)
    new_password2 = CharField(label='confirm new password', write_only=True)
    msg = CharField(allow_blank=True, read_only=True)
    recaptcha = ReCaptchaField(write_only=True)
    
    class Meta:
        model = User
        fields = ('new_password', 'new_password2', 'token', 'msg', 'recaptcha')
    
    def validate(self, data):
        new_password = data['new_password']
        new_password2 = data['new_password2']
        
        errors = []
        try:
            validators.validate_password(password=new_password)#, user=self.instance)
        except exceptions.ValidationError as e:
            errors = list(e.messages)
        if errors:
            raise ValidationError({'new_password':errors})
        
        if new_password2 != new_password:
            raise ValidationError({'new_password2' : ["رمز عبور مطابقت ندارد"]})
        return data
    
    def save(self):
        user = self.context.get('user', None)
        password = self.validated_data["new_password"]
        
        user.set_password(password)
        user.save()
        
        self.validated_data['token'] = generate_jwt_token(user)
        self.validated_data['msg'] = 'رمز عبور با موفقیت تغییر یافت.'
        
        return self.validated_data


class ProfileSerializer(ModelSerializer):
    #user = UserDetailSerializer()#read_only=True)
    #username = CharField(source='user.username')
    class Meta:
        model = Profile
        fields = [
                  #'username',
                  'cellphone', 
                  'birthdate', 
                  'telephone', 
                  'gender', 
                  'nationalID', 
                  'address', 
                  'postal_code', 
                  'magazine',
                  ]


''' use it in other serializers as:   UserSerializer(read_only) '''
class UserSerializer(ModelSerializer):
    profile = ProfileSerializer() #(write_only=True)
    class Meta:
        model = User
        fields = ['username', 
                  'first_name', 
                  'last_name',
                  'profile',
                 ]
        extra_kwargs = {"username":
                            {"read_only": True},
                        }
    
    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', {})
        profile = instance.profile
        instance = super(UserSerializer, self).update(instance, validated_data)
        
        #!!the following line updates the instance but does not return the updated json!!
        #Profile.objects.filter(user=instance).update(**profile_data)
        profile.cellphone = profile_data['cellphone']
        profile.birthdate = profile_data['birthdate']
        profile.telephone = profile_data['telephone']
        profile.gender = profile_data['gender']
        profile.nationalID = profile_data['nationalID']
        profile.address = profile_data['address']
        profile.postal_code = profile_data['postal_code']
        profile.magazine = profile_data['magazine']
        profile.last_modified = timezone.now()
        profile.save()
        
        return instance

class ChangeUserEmailSerializer(ModelSerializer):
    recaptcha = ReCaptchaField(write_only=True)
    email = EmailField(label='new email')
    email2 = EmailField(label='confirm email', write_only=True)
    
    class Meta:
        model = User
        fields = ('username', 'email', 'email2', 'recaptcha')
        read_only_fields = ['username', 'email',]
        #read_only_fields = ['user','plan', 'createdTime']
    
    def validate(self, data):
        
        user = self.context.get('user', None)
        
        email =  data.get('email',None).lower()
        email2 = data.get('email2',None).lower()
        
        user_qs = User.objects.filter(username=email)
        if user_qs.exists():# and :
            raise ValidationError("پست الکترونیک قبلا در سیسیتم ثبت شده است. لطغا مجددا تلاش نمایید.")
        if email2 != email:
            raise ValidationError({'email2' : ["پست الکترونیک مطابقت ندارد"]})
        data['email'] = email #normalized email
        return data
    
    #===========================================================================
    # def update(self, instance, validated_data):
    #     #instance = super(ChangeUserEmailSerializer, self)
    #     
    #     print(instance)
    #     return instance
    #===========================================================================


