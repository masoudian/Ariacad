# -*- coding: utf-8 -*-
# Generated by Django 1.11.8 on 2018-03-10 05:34
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user_type', models.CharField(default=0, max_length=100)),
                ('telephone', models.CharField(max_length=100)),
                ('cellphone', models.CharField(blank=True, max_length=100, null=True)),
                ('address', models.CharField(blank=True, max_length=500, null=True)),
                ('postal_code', models.CharField(blank=True, max_length=20)),
                ('gender', models.CharField(choices=[('f', 'زن'), ('m', 'مرد')], max_length=1)),
                ('nationalID', models.CharField(max_length=100)),
                ('birthdate', models.DateField(max_length=100, null=True)),
                ('cardNumber', models.CharField(max_length=100, null=True)),
                ('magazine', models.BooleanField(default=False)),
                ('virtualUser', models.BooleanField(default=False)),
                ('CompanyName', models.CharField(max_length=300)),
                ('economic_code', models.CharField(max_length=300)),
                ('national_identifier', models.CharField(max_length=300)),
                ('register_number', models.CharField(max_length=300)),
                ('centralOfficeAddress', models.TextField()),
                ('email_confirmed', models.BooleanField(default=False)),
                ('last_modified', models.DateTimeField(null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
