#!/usr/bin/python
# -*- coding: UTF-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user=models.OneToOneField('auth.User', on_delete=models.CASCADE)
    #first_name=models.CharField(max_length=30, null=True)    #redundant -- also in django.user
    #last_name=models.CharField(max_length=30, null=True)
    #email=models.EmailField(null=True)
    #password=models.CharField(max_length=20, null=True)
    user_type=models.CharField(max_length=100, default=0)
    telephone=models.CharField(max_length=100)
    cellphone=models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=500, blank=True, null=True) #added by sol
    postal_code=models.CharField(max_length=20, blank=True,)
    
    GENDER_CHOICES = (
       ('f', 'زن'),
       ('m', 'مرد'), 
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    nationalID=models.CharField(max_length=100)
    birthdate=models.DateField(max_length=100, null=True)
    cardNumber=models.CharField(max_length=100, null=True)
    magazine=models.BooleanField(default=False)
    virtualUser=models.BooleanField(default=False)
    CompanyName=models.CharField(max_length=300)
    economic_code=models.CharField(max_length=300)
    national_identifier=models.CharField(max_length=300)
    register_number=models.CharField(max_length=300)
    centralOfficeAddress=models.TextField()
#    signup_date=models.DateTimeField(null=True)
    email_confirmed = models.BooleanField(default=False)
    
    last_modified = models.DateTimeField(null=True)
    
    def __str__(self):  
        return "%s's profile" % self.user  
'''
@receiver(post_save, sender='auth.User')
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender='auth.User')
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
'''        